/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ie.tcd.cs.nembes.coror;

import ie.tcd.cs.nembes.coror.test.Debugger;
import ie.tcd.cs.nembes.coror.graph.Graph;
import ie.tcd.cs.nembes.coror.graph.Node;
import ie.tcd.cs.nembes.coror.graph.Triple;
import ie.tcd.cs.nembes.coror.graph.impl.GraphImpl;
import ie.tcd.cs.nembes.coror.rdf.model.GraphWriter;
import ie.tcd.cs.nembes.coror.rdf.model.impl.RDFAxiomWriter;
import ie.tcd.cs.nembes.coror.reasoner.InfGraph;
import ie.tcd.cs.nembes.coror.reasoner.rulesys.BasicForwardRuleInfGraph;
//import ie.tcd.cs.nembes.coror.reasoner.rulesys.RETERuleInfGraph;
import ie.tcd.cs.nembes.coror.reasoner.rulesys.Rule;
import ie.tcd.cs.nembes.coror.graph.enh.TemporalRETERuleInfGraph;
import ie.tcd.cs.nembes.coror.reasoner.rulesys.impl.ReasonerConfig;
import ie.tcd.cs.nembes.coror.shared.CororException;
import ie.tcd.cs.nembes.coror.reasoner.rulesys.selective.OwlRuleComposer;
import ie.tcd.cs.nembes.coror.reasoner.rulesys.selective.RuleInfo;
import ie.tcd.cs.nembes.coror.util.ArrayList;
import ie.tcd.cs.nembes.coror.util.iterator.ExtendedIterator;
import java.io.*;

/**
 *
 * @author WEI TAI
 */
public class Coror {
    
    /** A model that allows ontology access */
//    private Model model;
    
    private InfGraph infGraph;
    
    private Graph ontGraph;
        
    private ArrayList rules;
    
    
    /**
     * Start Coror from commandline
     */
    public static void main(String[] args) {
        Coror reasoner = new Coror("C:/Users/Dylan/Dropbox/Fifth Year/Dissertation/Codebase/Coror 20130904 Dylan/Coror/resources/reasoner.config");         
        reasoner.startReasoner();

        // this code block is for testing coror against owl test cases
//        boolean result = true;
//        Graph conclusion = reasoner.readData(ReasonerConfig.ontology.replaceAll("premise", "conclusion"), ReasonerConfig.ontFormat, true);
//        for(ExtendedIterator it = conclusion.find(Node.ANY, Node.ANY, Node.ANY); it.hasNext(); ){
//            Triple t = (Triple)it.next();
//            System.err.println(t);
//            if(!reasoner.infGraph.contains(t)){
//                result = false; break;
//            }
//        } 
//        if(result == true) System.err.println("success");
//        else System.err.println("fail");
        
//        // this code block is for outputing inference results
//        for(ExtendedIterator it = reasoner.getInfGraph().find(Node.ANY, Node.ANY, Node.ANY); it.hasNext();){
//            System.err.println(it.next() + " .");
//        }
        
    }
    
    public Coror(String configFile){
        loadConfiguration(configFile);
        ontGraph = readData(ReasonerConfig.ontology, ReasonerConfig.ontFormat, ReasonerConfig.ignoreAxioms);
        rules = loadRules();
    }
    
    protected final void loadConfiguration(String configFile){
        // read reasoner configurations
//        BufferedReader configReader = new BufferedReader(new InputStreamReader(Coror.class.getResourceAsStream(configFile)));
        BufferedReader configReader;
        try {
            configReader = new BufferedReader(new FileReader(configFile));
        } catch (FileNotFoundException ex) {
            throw new CororException("No such file exist for "+configFile);
        }
        ReasonerConfig.readConfig(configReader);
        try {
            configReader.close();
        } catch (IOException ex) {}        
    }
    
    /**
     * startReasoner engine to do an once-off reasoning
     */
    public void startReasoner (){        

        // bind a graph to the reasoner or rebind a changed graph       
        infGraph = ReasonerConfig.timeStamp? new TemporalRETERuleInfGraph(rules, null, ontGraph) : new BasicForwardRuleInfGraph(rules, null, ontGraph);
        Debugger.println(Debugger.GENERAL_INFO, " Original ontology size: "+(infGraph.size() -  infGraph.getDeductionsGraph().size())+" triples.");        
        // Start reasoning
        long startTime = System.nanoTime();
        infGraph.prepare();
        long elapsedTime = (System.nanoTime() - startTime) / 1000;
        
        Debugger.println(Debugger.PERFORMANCE_INFO, " Reasoning time: "+(elapsedTime)+" ms.");
        Debugger.println(Debugger.PERFORMANCE_INFO, " Memory usage: "+Debugger.getMemUsage()+" Byte.");
        Debugger.println(Debugger.GENERAL_INFO, " Original ontology size: "+(infGraph.size() -  infGraph.getDeductionsGraph().size())+" triples.");
        Debugger.println(Debugger.GENERAL_INFO, " Deduced triple size: "+infGraph.getDeductionsGraph().size()+" triples.");
        Debugger.println(Debugger.PERFORMANCE_INFO_LV2, " NoJ_All = "+ Debugger.NoJ_All + " NoSJ_All =  "+Debugger.NoSJ_All);
        Debugger.println(Debugger.PERFORMANCE_INFO_LV2, " NoM_All = "+ Debugger.NoM_All + " NoSM_All =  "+Debugger.NoSM_All);
        Debugger.println(Debugger.PERFORMANCE_INFO, (elapsedTime)+" "+
                Debugger.getMemUsage()+" " + 
                (infGraph.size() -  infGraph.getDeductionsGraph().size()) + " " +
                infGraph.getDeductionsGraph().size() + " " +
                Debugger.NoJ_All + " " + Debugger.NoSJ_All + " " + Debugger.NoM_All + " " + Debugger.NoSM_All + " " + Debugger.NoT_All);
    }
    
    
    
    public final Graph readData(String ontology, String format, boolean ignoreAxioms){
        
        Graph g = new GraphImpl();
        
        GraphWriter graphWriter = new GraphWriter(g);
        
        if(!ignoreAxioms) RDFAxiomWriter.writeAxioms(ontGraph);
                   
        if(format.equals("N-TRIPLE")) {
            try {
                graphWriter.readNTriple(new BufferedInputStream(new FileInputStream(ontology)));
            } catch (IOException ex) {
                throw new CororException("An error occured while reading the ontology " + ontology);
            } 
        }
        else{
            throw new CororException("The language" + format +" is supported as input");
        }
        
        return g;
    }
    
    public final ArrayList loadRules(){
        
        // run selective rule loading algorithm and calculate the rules to be excluded
        if(ReasonerConfig.selectiveRuleLoading){
            OwlRuleComposer composer = new OwlRuleComposer(ontGraph);
            ArrayList unselectedRules = composer.getUnselectedRules();
            Debugger.printAll(Debugger.RULE_INFO, "unselected rules", unselectedRules.iterator());
            if(!unselectedRules.isEmpty()){
                if(ReasonerConfig.excludeRules == null) ReasonerConfig.excludeRules = new ArrayList();
                for(int i=0; i<unselectedRules.size(); i++){
                    ReasonerConfig.excludeRules.add(((RuleInfo)unselectedRules.get(i)).getRuleName());
                }
            }
        }
        
        // generate a selective rule set
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(ReasonerConfig.ruleSet));
        } catch (FileNotFoundException ex) {
            System.err.println("No such a file exist for "+ReasonerConfig.ruleSet);
        }
        ArrayList rules;
        if(ReasonerConfig.excludeRules == null){
            rules = Rule.parseRules(Rule.rulesParserFromReader(br));
            
        }
        else{
            rules = Rule.parseRules(Rule.rulesParserFromReader(br), ReasonerConfig.excludeRules);
            
        }
        try {
            br.close();
        } catch (IOException ex) {}
        
        return rules;
    }
    
    /**
     * add a statement into the reasoner. Inference will be performed automatically
     * if the reasoner is started.
     */
    public void addTriple(Triple t){
        if(infGraph != null){
            ((BasicForwardRuleInfGraph)infGraph).performAdd(t);
        }
        else
            ontGraph.add(t);        
    }
    
    /**
     * remove a triple from the reasoner. Inference will be performed automatically
     * if the reasoner is started.
     */
    public void removeTriple(Triple t){
        ((BasicForwardRuleInfGraph)infGraph).performDelete(t);
        if(infGraph != null) {
            infGraph.rebind(ontGraph);
            infGraph.prepare();
        }
     }
    
    
    
    /**
     * Sweep the temporal triples as well as T-PBVs with a time stamp within the
     * given time slot. This method is only useful under the temporal mode.
     */
    public void sweepReasoner(long start, long end){
//        if(ReasonerConfig.reasonerMode == ReasonerConfig.REASONER_MODE_TEMPORAL){
//            ((TemporalRETERuleInfGraph)infGraph).sweepGraph(start, end);
//        }
//        else
//            throw new CororException ("sweep reasoner is only supported for temporal engine");
        throw new CororException("method not yet implemented");
            
    }
    
    
    public InfGraph getInfGraph(){
        return infGraph;
    }
    
    /**
     * Stop a reasoner. 
     */
    public void stopReasoner(){
        infGraph.close();
        infGraph = null;
        ontGraph = null;
        rules = null;
    }
}
