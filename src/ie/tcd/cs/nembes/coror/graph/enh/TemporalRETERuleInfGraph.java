/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ie.tcd.cs.nembes.coror.graph.enh;

import ie.tcd.cs.nembes.coror.graph.Graph;
import ie.tcd.cs.nembes.coror.reasoner.rulesys.BasicForwardRuleInfGraph;
import ie.tcd.cs.nembes.coror.reasoner.rulesys.impl.TemporalFRuleEngineI;
import ie.tcd.cs.nembes.coror.util.ArrayList;
import ie.tcd.cs.nembes.coror.util.iterator.ExtendedIterator;

/**
 * Extends the RETERuleInfGraph with the capability to handle temporal triples
 * @author WEI TAI
 */
public class TemporalRETERuleInfGraph extends BasicForwardRuleInfGraph {
    
//    protected FGraph ftempdeductions;
    
    ////////////////////////////////////////////////////////
    //  Constructors are the same as its superclass
    ////////////////////////////////////////////////////////
    
    public TemporalRETERuleInfGraph(Graph schema) {
        super(schema);
    }
    
    public TemporalRETERuleInfGraph(ArrayList rules, Graph schema) {
        super(rules, schema);
    }
    
    public TemporalRETERuleInfGraph(ArrayList rules, Graph schema, Graph data) {
         super(rules, schema, data);
    }
    
    /**
     * sweep the inferred triples as well as the T-PBVs in RETE network within 
     * the given time slot.
     * @param start start of the time slot
     * @param end end of the time slot
     */
    public void sweepGraph(long start, long end){
        ExtendedIterator it = getDeductionsGraph().find(null, null, null);
        // remove all temporal triples in the deduction graph
        if(it.hasNext()){
            Object next = it.next();
            if(next instanceof TemporalTriple){
                ((TemporalTriple) next).within(start, end);
                getDeductionsGraph().delete((TemporalTriple)next);
            }
        }
        ((TemporalFRuleEngineI)engine).sweepRETE(start, end);
    }
    
//    public synchronized void performAddTemporal(TemporalTriple t){
//        
//    }
}
