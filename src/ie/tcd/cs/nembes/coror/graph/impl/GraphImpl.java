/*
  (c) Copyright 2002, 2003, 2004, 2005, 2006, 2007 Hewlett-Packard Development Company, LP
  [See end of file]
  $Id: GraphImpl.java,v 1.59 2007/01/02 11:52:20 andy_seaborne Exp $
 */

package ie.tcd.cs.nembes.coror.graph.impl;

import ie.tcd.cs.nembes.coror.graph.Axiom;
import ie.tcd.cs.nembes.coror.graph.Node;
import ie.tcd.cs.nembes.coror.graph.Triple;
import ie.tcd.cs.nembes.coror.util.ArrayList;
import ie.tcd.cs.nembes.coror.util.iterator.ExtendedIterator;
import ie.tcd.cs.nembes.coror.util.iterator.IteratorImpl;
import ie.tcd.cs.nembes.coror.util.iterator.WrappedIterator;
import java.util.Vector;
import ie.tcd.cs.nembes.coror.util.Iterator;
import ie.tcd.cs.nembes.coror.shared.CororException;

/**
 * A memory-backed graph with S/P/O indexes.
 * @author  bwm, kers
 */
public class GraphImpl extends GraphBase {
    /**
     * The number-of-times-opened count.
     */
    protected int count;
    
    /**
     * triples
     */
//    protected Vector triples = new Vector(10,10);
    protected ArrayList triples = new ArrayList(100);
    
    public GraphImpl() {
        super();
        count = 1;
    }
        
    public void performAdd( Triple t ) {
        if(!this.contains(t)) {
//	    triples.addElement(t);
            triples.add(t);
	    cacheTriple(t);
        }
    }
    
    private void cacheTriple(Triple t) {
	cacheNode(t.getSubject());
	cacheNode(t.getPredicate());
	cacheNode(t.getObject());
    }
    
    private boolean cacheNode(Node n) {
	if(n.isURI()) {
	    nodes.put(n.getURI(), n);
	    return true;
	} else {
	    if(n.isBlank()) {
		nodes.put(n.getBlankNodeLabel(), n);
		return true;
	    } else {
		return false;
	    }
	}
    }
    
    public void performDelete( Triple t ) {
	performDelete(t, false);
    }
    
    @Override
    public void performDelete( Triple t, boolean removeAxiom ) {
        if(removeAxiom || !(t instanceof Axiom)){
//            triples.removeElement(t);
            triples.remove(t);
        }
    }

    public int graphBaseSize() {
	return triples.size();
    }
    
    /**
     * Answer an ExtendedIterator over all the triples in this graph that match the
     * triple-pattern <code>m</code>. Delegated to the store.
     */
    public ExtendedIterator graphBaseFind( Triple m ) {
//        System.err.println("check point in GraphImpl::graphBaseFind");
	Vector newVector = new Vector();
//        ArrayList array = new ArrayList();
//	Iterator it = new IteratorImpl(triples);
        Iterator it = triples.iterator();
	Triple aus;
	while(it.hasNext()) {
	    aus = (Triple)(it.next());
	    if(aus.matches((Triple) m))
		newVector.addElement(aus);
//                array.add(m);
	}

	IteratorImpl result = new IteratorImpl(newVector);
//        Iterator result = array.iterator();
        
//        System.err.println();
        
	return WrappedIterator.create(result);
    }
    
    /**
     * Answer true iff this graph contains <code>t</code>. If <code>t</code>
     * happens to be concrete, then we hand responsibility over to the store.
     * Otherwise we use the default implementation.
     */
    public boolean graphBaseContains( Triple t ) {
//	return isSafeForEquality( t )
//	    ? graphBaseFind(t).hasNext()
//	    : super.graphBaseContains( t );
        if(t == null) throw new CororException("cannot search for a null triple");
        for(Iterator i = triples.iterator(); i.hasNext();){
           if(t.matches((Triple)i.next())) return true;
        }
        return false;
    }
    
    /**
     * Clear this GraphImpl, ie remove all its triples (delegated to the store).
     */
    public void clear() {
//	triples.removeAllElements();
//	triples.trimToSize();
        triples.clear();
    }
    
    /**
     * Sub-classes over-ride this method to release any resources they no
     * longer need once fully closed.
     */
    protected void destroy() {
//	triples.setSize(0);
        triples.clear();
    }
    
    /**
     * Note a re-opening of this graph by incrementing the count. Answer
     * this Graph.
     */
    public GraphBase openAgain() {
	count += 1;
	return this;
    }
    
    /**
     * Close this graph; if it is now fully closed, destroy its resources and run
     * the GraphBase close.
     */
    public void close() {
	if (--count == 0) {
	    destroy();
	    super.close();
	}
    }
    
    /**
     * Answer true iff this triple can be compared for sameValueAs by .equals(),
     * ie, it is a concrete triple with a non-literal object.
     */
    protected final boolean isSafeForEquality( Triple t ) {
	return t.isConcrete() && !t.getObject().isLiteral();
    }
}

/*
 *  (c) Copyright 2002, 2003, 2004, 2005, 2006, 2007 Hewlett-Packard Development Company, LP
 *  All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */