package ie.tcd.cs.nembes.coror.graph.impl;

import ie.tcd.cs.nembes.coror.graph.BulkUpdateHandler;
import ie.tcd.cs.nembes.coror.graph.Graph;
import ie.tcd.cs.nembes.coror.graph.GraphUtil;
import ie.tcd.cs.nembes.coror.graph.Node;
import ie.tcd.cs.nembes.coror.graph.Triple;
import ie.tcd.cs.nembes.coror.shared.CororException;
import ie.tcd.cs.nembes.coror.util.Iterator;
import ie.tcd.cs.nembes.coror.util.IteratorCollection;
import ie.tcd.cs.nembes.coror.util.ArrayList;
import ie.tcd.cs.nembes.coror.util.iterator.ExtendedIterator;

/**
A simple-minded implementation of the bulk update interface. This only
operates on (subclasses of) GraphBase, since it needs access to the
performAdd/performDelete operations.
<p>
<b>Comments by Wei</b>: GraphEventManager is removed
<p>
It handles update events, with a special eye to not copying iterators unless
there is at least one listener registered with the graph's event manager.

	@author kers
*/

public class SimpleBulkUpdateHandler implements BulkUpdateHandler
{
protected GraphBase graph;
//protected GraphEventManager manager;

public SimpleBulkUpdateHandler( GraphBase graph )
    { 
    this.graph = graph; 
//    this.manager = graph.getEventManager();
    }

public void add( Triple [] triples )
    { 
    for (int i = 0; i < triples.length; i += 1) graph.performAdd( triples[i] ); 
//   manager.notifyAddArray( graph, triples );
    }
    
public void add( ArrayList triples )
    { add( triples, true ); }
    
protected void add( ArrayList triples, boolean notify )
    {
    for (int i = 0; i < triples.size(); i += 1) graph.performAdd( (Triple) triples.get(i) ); 
//    if (notify) manager.notifyAddList( graph, triples );
    }

public void add( Iterator it )
    { addIterator( it, true ); }

public void addIterator( Iterator it, boolean notify )
    { 
    ArrayList s = IteratorCollection.iteratorToList( it );
    add( s, false );
//    if (notify) manager.notifyAddIterator( graph, s );
    }
    
public void add( Graph g )
    { add( g, false ); }
    
public void add( Graph g, boolean withReifications )
    { 
    addIterator( GraphUtil.findAll( g ), false );  
    }

public void delete( Triple [] triples )
    { 
    for (int i = 0; i < triples.length; i += 1) graph.performDelete( triples[i] ); 
    }

public void delete( ArrayList triples )
    { delete( triples, true ); }
    
protected void delete( ArrayList triples, boolean notify )
    { 
    for (int i = 0; i < triples.size(); i += 1) graph.performDelete( (Triple) triples.get(i) );
    }

public void delete( Iterator it )
    { deleteIterator( it, true ); }
    
public void deleteIterator( Iterator it, boolean notify )
    {  
    ArrayList L = IteratorCollection.iteratorToList( it );
    delete( L, false );
//    if (notify) manager.notifyDeleteIterator( graph, L );
     }
     
private ArrayList triplesOf( Graph g )
    {
    ArrayList L = new ArrayList();
    Iterator it = g.find( Triple.ANY );
    while (it.hasNext()) L.add( it.next() );
    return L;
    }
        
public void delete( Graph g )
    { delete( g, false ); }
    
public void delete( Graph g, boolean withReifications )
    { 
    if (g.dependsOn( graph ))
        delete( triplesOf( g ) );
    else
        deleteIterator( GraphUtil.findAll( g ), false );
//    if (withReifications) deleteReifications( graph, g );
//    manager.notifyDeleteGraph( graph, g );
    }

public void removeAll()
    { removeAll( graph ); 
//    notifyRemoveAll(); 
    }

//protected void notifyRemoveAll()
//    { manager.notifyEvent( graph, GraphEvents.removeAll ); }

public void remove( Node s, Node p, Node o )
    { removeAll( graph, s, p, o ); 
//    manager.notifyEvent( graph, GraphEvents.remove( s, p, o ) ); 
    }

public static void removeAll( Graph g, Node s, Node p, Node o )
    {
    ExtendedIterator it = g.find( s, p, o );
    try { 
    	while (it.hasNext()) { 
	    	it.next(); 
		    try {
					it.remove();
			} catch (CororException e) {
				e.printStackTrace();
			} 
    	} 
    }
    finally { it.close(); }
    }

public static void removeAll( Graph g )
    {
    ExtendedIterator it = GraphUtil.findAll( g );
    try { 
    	while (it.hasNext()) { 
    		it.next(); 
		    try {
				it.remove();
			} catch (CororException e) {
				e.printStackTrace();
			} 
    	} 
    }
    finally { it.close(); }
    }
}