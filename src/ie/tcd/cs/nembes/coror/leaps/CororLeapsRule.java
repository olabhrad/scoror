package ie.tcd.cs.nembes.coror.leaps;

import ie.tcd.cs.nembes.coror.graph.Node;
import ie.tcd.cs.nembes.coror.graph.Triple;
import ie.tcd.cs.nembes.coror.reasoner.TriplePattern;
import ie.tcd.cs.nembes.coror.reasoner.rulesys.Node_RuleVariable;
import ie.tcd.cs.nembes.coror.reasoner.rulesys.Rule;
import ie.tcd.cs.nembes.coror.util.MultiArrayMap;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;

import org.drools.leaps.LeapsCondition;
import org.drools.leaps.LeapsConsequence;
import org.drools.leaps.LeapsDeclaration;
import org.drools.leaps.LeapsDeclarationImpl;
import org.drools.leaps.LeapsFactHandle;
import org.drools.leaps.LeapsFactTable;
import org.drools.leaps.LeapsNegativeCondition;
import org.drools.leaps.LeapsNoMatchesFoundException;
import org.drools.leaps.LeapsToken;
import org.drools.leaps.LeapsWorkingMemory;
import org.drools.leaps.rule.Importer;
import org.drools.leaps.rule.LeapsInvalidRuleException;
import org.drools.leaps.rule.LeapsNoConsequenceException;
import org.drools.leaps.rule.LeapsNoParameterDeclarationException;
import org.drools.leaps.rule.LeapsRuleSet;
import org.drools.leaps.util.LeapsTableIterator;

public class CororLeapsRule extends Rule{
	final int SUBJECT = 0,PREDICATE = 1;final int OBJECT = 2;

	public CororLeapsRule(ie.tcd.cs.nembes.coror.util.ArrayList head, 
			ie.tcd.cs.nembes.coror.util.ArrayList body) {
		super(head, body);
	}
	public CororLeapsRule(String name, ie.tcd.cs.nembes.coror.util.ArrayList head, 
			ie.tcd.cs.nembes.coror.util.ArrayList body) {
		super(name, head, body);
	}
	
	//----TEST CONSTRUCTOR TO BE REMOVED-----
	public CororLeapsRule(String name) {
		super(new ie.tcd.cs.nembes.coror.util.ArrayList(), new ie.tcd.cs.nembes.coror.util.ArrayList());
		this.name = name;
	}
	
	public void convertToLeapsFormat(ie.tcd.cs.nembes.coror.util.ArraySet predicatesUsed) throws LeapsInvalidRuleException{
	
		final LeapsDeclaration[] decs = new LeapsDeclaration[this.bodyLength()];
		//Add Condition Elements
		for (int i = 0; i < this.bodyLength(); i++) {
            Object clause = this.getBodyElement(i);
            TriplePattern pattern = null;
//System.out.println("clause: "+clause);
            if (clause instanceof TriplePattern) {
            //	System.out.println("NEW CLAUSE: "+clause);
            	pattern = (TriplePattern)clause;
            	Node subject = pattern.getSubject();
        		Node predicate = pattern.getPredicate();
        		Node object = pattern.getObject();
        		
        		if(subject.isVariable()){
        			varReferences.put(getVariableIndex(subject), new Pair(i,SUBJECT));
        		}
        		if(predicate.isVariable()){
        			varReferences.put(getVariableIndex(predicate), new Pair(i,PREDICATE));
                    LEAPSEngine.wildcardRule = true;
        		}else {
        			if (! LEAPSEngine.wildcardRule) {
        				predicatesUsed.add(predicate);
        			}
        		}
        		if(object.isVariable()){
        			varReferences.put(getVariableIndex(object), new Pair(i,OBJECT));
        		}
                
                decs[i] = this.addParameterDeclaration(""+i, Triple.class);
                
            }
            
            final int index = i; final TriplePattern patternf = pattern;
        	this.addCondition(new LeapsCondition() {
    			public boolean isAllowed() throws Exception {
    				Triple trip = (Triple) decs[index].getValue();
    				//System.out.println(trip.getSubject() + ", "+
    				//				   trip.getPredicate() + ", " +
    				//				   trip.getObject() );
    				boolean test = checkMatching(patternf,trip);
    			    //System.out.println("index: "+index+", "+test);
    				return test;
    			}
    			public ArrayList<LeapsDeclaration> getRequiredDeclarations() {
    				ArrayList<LeapsDeclaration> l = new ArrayList<LeapsDeclaration>();
    				l.add(decs[index]);
    				return l;
    			}
    		}
    		);
		}
		
		for(int k=0;k<numVars;k++){
            ie.tcd.cs.nembes.coror.util.Iterator it = (ie.tcd.cs.nembes.coror.util.Iterator) varReferences.getAll(k);
            it.next(); // if the  variable has more than one references in the body
            if(it.hasNext()){
            	it.reset();
            	addVariableCondition(k,it,decs);
            }
        }

		final CororLeapsRule finalt = this;
		this.setConsequence(new LeapsConsequence() {
			public void invoke() throws Exception {
				//System.err.println("SUCCESS");
				
				for (int i = 0; i < finalt.headLength(); i++) {
		            Object clause = finalt.getHeadElement(i);
		            TriplePattern pattern = null;
		            if (clause instanceof TriplePattern) {
		            	pattern = (TriplePattern) clause;
		            	Node subject = pattern.getSubject();
		        		Node predicate = pattern.getPredicate();
		        		Node object = pattern.getObject();
		        		
		        		if(subject.isVariable()){
		        			subject = (Node) varBindings.get(getVariableIndex(subject));
		        		}
		        		if(predicate.isVariable()){
		        			predicate = (Node) varBindings.get(getVariableIndex(predicate));
		        		}
		        		if(object.isVariable()){
		        			object = (Node) varBindings.get(getVariableIndex(object));
		        		}
		            	Triple t = new Triple(subject,predicate,object);
		            	LEAPSEngine.addTriple(t,true);
		            	
		            }
		        }
			}
		});
	}
	
	private void addVariableCondition(final int index, final ie.tcd.cs.nembes.coror.util.Iterator it, final LeapsDeclaration[] decs) throws LeapsInvalidRuleException{
		
		//final Pair p1 = (Pair) it.next();
		
		//if(!it.hasNext()){
		//	return;
		//}
		final ArrayList<LeapsDeclaration> leapsDecs = new ArrayList<LeapsDeclaration>();
		
		//obtain triple declarations needed
		while(it.hasNext()){
    		Pair p = (Pair) it.next();
    		leapsDecs.add(decs[p.ceIndex]);
    	}
		it.reset();
		
		this.addCondition(new LeapsCondition() {
			public boolean isAllowed() throws Exception {
				
				boolean ret = true;
				//System.out.println("P1.a: "+decs[p1.a]);
				it.reset();
				Pair p = (Pair) it.next();
				Object last = getTripleAttributeValue((Triple) decs[p.ceIndex].getValue(), p.atrributeIndex);
				//System.out.println("P.a: "+index+", "+decs[p.ceIndex]+", "+p.atrributeIndex);
				
				//ensure that all variable references have the same value
				while(it.hasNext() && ret){
            		p = (Pair) it.next();
            		//System.out.println("P.a: "+index+", "+decs[p.ceIndex]+", "+p.atrributeIndex);
            		Object current = getTripleAttributeValue((Triple) decs[p.ceIndex].getValue(), p.atrributeIndex);
            		ret &= last.equals(current);
            		last = current;
            	}
				if(ret){
					//System.out.println("index: "+index+", node: "+last);
					varBindings.put(index, last);
				}
				//System.out.println(trip.getSubject() + ", "+
				//				   trip.getPredicate() + ", " +
				//				   trip.getObject() );
				//System.out.println("index: "+index+", "+trip.getPredicate() + ", "+predicatef);
				return ret;//checkMatching(pattern,trip);
			}
			public ArrayList<LeapsDeclaration> getRequiredDeclarations() {
				return leapsDecs;
			}
		}
		);
	}
	
	private Object getTripleAttributeValue(Triple triple, int index){
		switch(index){
			case 0:
				return triple.getSubject();
			case 1:
				return triple.getPredicate();
			case 2:
				return triple.getObject();
			default:
				return null;
		}
	}
	
	private boolean checkMatching(TriplePattern pattern, Triple fact){
		Node subject = pattern.getSubject();
		Node predicate = pattern.getPredicate();
		Node object = pattern.getObject();
		
		boolean s = true,p = true,o = true;
		
		if(subject.isVariable()){
			varBindings.put(getVariableIndex(subject), fact.getSubject()) ;
		}else{s = fact.getSubject().equals(pattern.getSubject());}
		if(!s){return false;}
	
		if(predicate.isVariable()){
			varBindings.put(getVariableIndex(predicate), fact.getPredicate()) ;
		}else{p = fact.getPredicate().equals(pattern.getPredicate());}
		if(!p){return false;}
		
		if(object.isVariable()){
			varBindings.put(getVariableIndex(object), fact.getObject()) ;
		}else{o =  fact.getObject().equals(pattern.getObject());}
		if(!o){return false;}
		//o = object.isVariable()? true : fact.getObject().equals(pattern.getObject());
		
		//System.out.println(fact.getPredicate()+", "+pattern.getPredicate());
		
		return s && p && o;
	}
	
	private int getVariableIndex(Node n) {
       return ((Node_RuleVariable)n).getIndex();
    }
	
	private MultiArrayMap varReferences = new MultiArrayMap();
	private HashMap varBindings = new HashMap();
	
    /** The parent ruleSet */
    private LeapsRuleSet ruleSet;

	/** Formal parameter declarations. */
    private final java.util.ArrayList declarations = new java.util.ArrayList();

    /** Documentation. */
    private String documentation = "no documentation present";

    /** Salience value. */
    private int salience = 0;

    /** Consequence. */
    private LeapsConsequence consequence = null;

    private Importer importer;

    /** Load order in RuleSet */
    private long loadOrder;

    /**
     * Condition - positive. stored at the "top" declaration level it checked
     * when iterator gets to the "top" declation position
     */
    private final Hashtable positiveConditions = new Hashtable();

    /** Negative conditions map by declaration. */
    private final List negativeConditions = new java.util.ArrayList();

    private java.util.ArrayList iterators = new java.util.ArrayList();

    /**
     * Searches <code>Working Memory</code> for facts that would satisfy
     * <code>Conditions</code>.
     * 
     * @param memory
     *            The Working Memory.
     * @param token
     *            The token that cares information about dominant fact and
     *            current state of processing for this fact.
     */
    @SuppressWarnings("unchecked")
	public void seek(LeapsWorkingMemory memory, LeapsToken token)
            throws LeapsNoMatchesFoundException, Exception,
            LeapsInvalidRuleException {
        int numberOfDeclarations = declarations.size();
        // getting iterators first
        for (int i = 0; i < numberOfDeclarations; i++) {
            // there is no dominant fact for negative based searches
            if (token.getTokenType() == LeapsToken.ASSERTED
                    && i == token.getCurrentRuleHandle().getCePosition()) {
                this.iterators.set(i, memory.getLeapsFactTable(
                        ((LeapsDeclarationImpl) declarations.get(i)).getType())
                        .baseFactIterator(token.getDominantFactHandle()));
              //  System.out.println("Fixed at: "+i+", type is: "+((LeapsDeclarationImpl) declarations.get(i)).getType());
               // System.out.println(((LeapsTableIterator)this.iterators.get(i)).peekNext());     
                //Iterator currentIterator = (LeapsTableIterator) iterators.get(i);
                //           while(currentIterator.hasNext()){
                  //         	System.out.println(currentIterator.next());
                    //       }
            } else {
                this.iterators.set(i, memory.getLeapsFactTable(
                        ((LeapsDeclarationImpl) declarations.get(i)).getType())
                        .iterator(
                                token.getDominantFactHandle(),
                                (token.isResume() ? token.getFactHandle(i)
                                        : token.getDominantFactHandle())));
               // System.out.println("Iterator at: "+i+", type is: "+((LeapsDeclarationImpl) declarations.get(i)).getType());
    //            Iterator currentIterator = (LeapsTableIterator) iterators.get(i);
     //           while(currentIterator.hasNext()){
       //         	System.out.println(currentIterator.next());
         //       }
            }
            // init results
            ((LeapsDeclarationImpl) this.declarations.get(i))
                    .setIterator((LeapsTableIterator) this.iterators.get(i));
        }
        // check if any iterators are empty to abort
        // check if we resume and any facts disappeared
        boolean someIteratorsEmpty = false;
        boolean doReset = false;
        boolean skip = token.isResume();
        LeapsTableIterator currentIterator;
        for (int i = 0; i < numberOfDeclarations && !someIteratorsEmpty; i++) {
            currentIterator = (LeapsTableIterator) iterators.get(i);
            if (currentIterator.isEmpty()) {
                someIteratorsEmpty = true;
            } else {
                if (!doReset) {
                    if (skip
                            && !currentIterator.peekNext().equals(token.getFactHandle(i))) {
                        skip = false;
                        doReset = true;
                    }
                } else {
                    currentIterator.reset();
                }
            }

        }
        // check if one of them is empty and immediate return
        if (someIteratorsEmpty) {
        	// System.err.println("some of tables do not have facts \n" + token);
            throw new LeapsNoMatchesFoundException(
                    "some of tables do not have facts \n" + token);
        }
      
        
        // iterating
        int jj = 0;
        boolean found = false;
        boolean done = false;
        while (!done) {
            currentIterator = (LeapsTableIterator) iterators.get(jj);
            if (!currentIterator.hasNext()) {
                if (jj == 0) {
                    done = true;
                } else {
                    //                    
                    currentIterator.reset();
                    jj = jj - 1;
                    if (skip) {
                        skip = false;
                    }
                }
            } else {
                currentIterator.next();
                // check if match found
                if (this.evaluatePositiveConditions(jj)) {
                    // start iteratating next iterator
                    // or for the last one check negative conditions and fire
                    // consequence
                    if (jj == (numberOfDeclarations - 1)) {
                        if (!skip) {
                            // check for negative conditions
                            if (this.evaluateNegativeCondition(memory, token)) {
                                done = true;
                                found = true;
                                // System.out.println("FIRE: " + token);
                                this.consequence.invoke();
                                // store current state if to resume iterations
                                // here
                                for (int l = 0; l < iterators.size(); l++) {
                                    token.set(l,
                                         (LeapsFactHandle) ((LeapsTableIterator) iterators
                                            .get(l)).current());
                                }
                            }
                        } else {
                            skip = false;
                        }
                    } else {
                        jj = jj + 1;
                    }
                } else {
                    if (skip) {
                        skip = false;
                    }
                }
            }
        }
        if (!found) {
        	 // System.err.println("iteration did not find anything");
            throw new LeapsNoMatchesFoundException(
                    "iteration did not find anything");
          
        }
    }

    /**
     * 
     * Check if any conditions with max value of declaration index at this
     * position (<code>index</code>) are satisfied
     * 
     * @param index
     *            Position of the iterator that needs condition checking
     * @return success Indicator if all conditions at this position were
     *         satisfied.
     * @throws Exception
     */
    private boolean evaluatePositiveConditions(int index) throws Exception {
        boolean ret = true;
        if (this.positiveConditions.containsKey(new Integer(index))) {
        	java.util.ArrayList conds = (java.util.ArrayList) this.positiveConditions
                    .get(new Integer(index));
            for (int i = 0; i < conds.size() && ret; i++) {
            	//System.out.println("index: "+i+", "+conds.size());
                ret = ((LeapsCondition) conds.get(i)).isAllowed();
            }
        }
        return ret;
    }

    /**
     * Check if any of the negative conditions are satisfied, success when none
     * found
     * 
     * @param memory
     * @param token
     * @return success
     * @throws Exception
     */
    private boolean evaluateNegativeCondition(LeapsWorkingMemory memory,
            LeapsToken token) throws Exception {
        boolean notFound = true;
        // checking retracted negative that becomes positive
        if (token.getTokenType() == LeapsToken.RETRACTED) {
            notFound = ((LeapsNegativeCondition) this.negativeConditions
                    .get(token.getCurrentRuleHandle().getCePosition()))
                    .isAllowed(token.getDominantFactHandle().getObject());
            // .isAllowed(token.getDominantFactHandle().object);

        }
        if (notFound) {
            // checking all negative
            LeapsNegativeCondition negativeCondition;
            LeapsTableIterator tableIterator;
            LeapsFactTable table;
            for (Iterator it = this.negativeConditions.iterator(); it.hasNext()
                    && notFound;) {
                negativeCondition = (LeapsNegativeCondition) it.next();
                // 1. starting with regular tables
                // scan the whole table
                table = memory.getLeapsFactTable(negativeCondition.getType());
                tableIterator = table.iterator(table.getHeadObject());
                while (tableIterator.hasNext() && notFound) {
                    notFound = !negativeCondition
                            .isAllowed(((LeapsFactHandle) tableIterator.next())
                                    .getObject());
                    // .isAllowed(((LeapsFactHandle)
                    // tableIterator.next()).object);
                }
                if (notFound) {
                    // 2. checking shadow tables
                    // scan only higher facts
                    table = memory.getLeapsShadowFactTable(negativeCondition
                            .getType());
                    tableIterator = table.iterator(table.getHeadObject(), table
                            .getHeadObject(), token.getDominantFactHandle());
                    while (tableIterator.hasNext() && notFound) {
                        notFound = !negativeCondition
                                .isAllowed(((LeapsFactHandle) tableIterator
                                        .next()).getObject());
                        // .next()).object);
                    }
                }
            }
        }
        return notFound;
    }

    /**
     * Set the documentation.
     * 
     * @param documentation -
     *            The documentation.
     */
    public void setDocumentation(String documentation) {
        this.documentation = documentation;
    }

    /**
     * Retrieve the documentation.
     * 
     * @return The documentation or <code>null</code> if none.
     */
    /**
     * @return
     */
    public String getDocumentation() {
        return this.documentation;
    }

    public void checkValidity() throws LeapsInvalidRuleException {
        if (this.declarations.isEmpty()) {
            throw new LeapsNoParameterDeclarationException(this);
        }

        if (this.consequence == null) {
            throw new LeapsNoConsequenceException(this);
        }
    }

    public LeapsRuleSet getRuleSet() {
        return this.ruleSet;
    }

    /**
     * Retrieve the name of this rule.
     * 
     * @return The name of this rule.
     */
    public String getName() {
        return this.name;
    }

    /**
     * Retrieve the <code>Rule</code> salience.
     * 
     * @return The salience.
     */
    public int getSalience() {
        return this.salience;
    }

    /**
     * Set the <code>Rule<code> salience.
     *
     * @param salience The salience.
     */
    public void setSalience(int salience) {
        this.salience = salience;
    }

    public long getLoadOrder() {
        return loadOrder;
    }

    void setLoadOrder(long loadOrder) {
        this.loadOrder = loadOrder;
    }

    public Importer getImporter() {
        return this.importer;
    }

    public void setImporter(Importer importer) {
        this.importer = importer;
    }

    /**
     * Adds the <code>Declaration</code> for one of the rule variables.
     * 
     * @param identifier
     *            The declaration identifier.
     * @param class
     *            The variable type.
     * @return The declaration.
     */
    @SuppressWarnings("unchecked")
	public LeapsDeclaration addParameterDeclaration(String identifier, Class c)
            throws LeapsInvalidRuleException {
        if (this.existsParameterDeclaration(identifier)) {
            throw new LeapsInvalidRuleException(
                    "Attempt to add existing param name", this);
        }
        // add declaration
        this.declarations.add(this.declarations.size(),
                new LeapsDeclarationImpl(identifier, c, this.declarations
                        .size()));
        // add iterator position and its position
        this.iterators.add(this.iterators.size(), null);
        ((LeapsDeclarationImpl) this.declarations
                .get(this.declarations.size() - 1))
                .setIterator((LeapsTableIterator) this.iterators
                        .get(this.iterators.size() - 1));
        // return
        return (LeapsDeclarationImpl) this.declarations.get(this.declarations
                .size() - 1);
    }

    public void addNegativeCondition(LeapsNegativeCondition condition)
            throws LeapsInvalidRuleException {
        this.negativeConditions.add(condition);
    }

    private boolean existsParameterDeclaration(String identifier) {
        if (this.getNumberOfParameterDeclarations() > 0) {
            for (int i = 0; i < this.declarations.size(); i++) {
                if (((LeapsDeclarationImpl) this.declarations.get(i))
                        .getIdentifier().equals(identifier)) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * @param index
     * @return
     */
    public Class getParameterDeclarationType(int index) {
        return ((LeapsDeclarationImpl) this.declarations.get(index)).getType();
    }

    /**
     * @return
     */
    public int getNumberOfParameterDeclarations() {
        return this.declarations.size();
    }

    /**
     * @param pos
     * @return
     */
    public LeapsNegativeCondition getNegativeCondition(int pos) {
        return (LeapsNegativeCondition) this.negativeConditions.get(pos);
    }

    /**
     * @return
     */
    public List getNegativeConditions() {
        return this.negativeConditions;
    }

    /**
     * Set the <code>Consequence</code> that is associated with the successful
     * match of this rule.
     * 
     * @param consequence
     *            The <code>Consequence</code> to attach to this
     *            <code>Rule</code>.
     */
    public void setConsequence(LeapsConsequence consequence) {
        this.consequence = consequence;
    }

    public LeapsConsequence getConsequence() {
        return this.consequence;
    }

    /**
     * Set the <code>Condition</code> that is associated with the successful
     * match of this rule.
     * 
     * @param condition
     *            The <code>condition</code> to attach to this
     *            <code>Rule</code>.
     */
    public void addCondition(LeapsCondition condition)
            throws LeapsInvalidRuleException {
        // determining conditionTopDeclarations
        int topDeclarationIndex = -1;
        int indexOf = -1;
        ArrayList<LeapsDeclaration> requiredDeclartions = (ArrayList<LeapsDeclaration>) condition
                .getRequiredDeclarations();
        Iterator it = requiredDeclartions.iterator();
        while(it.hasNext()){
        	indexOf = this.declarations.indexOf(it.next());
              if (indexOf > topDeclarationIndex) {
                 topDeclarationIndex = indexOf;
              }
        }
       // for (int i = 0; i < requiredDeclartions.length; i++) {
         //   indexOf = this.declarations.indexOf(requiredDeclartions[i]);
          //  if (indexOf > topDeclarationIndex) {
           //     topDeclarationIndex = indexOf;
           // }
       // }
        if (topDeclarationIndex == -1) {
            throw new LeapsInvalidRuleException(
                    "No matching declarations for condition", this);
        }
        java.util.ArrayList conds = null;
        if (this.positiveConditions
                .containsKey(new Integer(topDeclarationIndex))) {
            conds = (java.util.ArrayList) this.positiveConditions.get(new Integer(
                    topDeclarationIndex));
        } else {
            conds = new java.util.ArrayList();
        }
        conds.add(condition);
        this.positiveConditions.put(new Integer(topDeclarationIndex), conds);
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    public String toString() {
        return this.name;
    }

    
}
