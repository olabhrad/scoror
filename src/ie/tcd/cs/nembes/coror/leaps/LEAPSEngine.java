package ie.tcd.cs.nembes.coror.leaps;

import org.drools.leaps.LeapsRuleBase;
import org.drools.leaps.LeapsWorkingMemory;
import org.drools.leaps.rule.LeapsDuplicateRuleNameException;
import org.drools.leaps.rule.LeapsInvalidRuleException;
import org.drools.leaps.rule.LeapsRuleSet;

import ie.tcd.cs.nembes.coror.graph.Node;
import ie.tcd.cs.nembes.coror.graph.Triple;
import ie.tcd.cs.nembes.coror.reasoner.Finder;
import ie.tcd.cs.nembes.coror.reasoner.TriplePattern;
import ie.tcd.cs.nembes.coror.reasoner.rulesys.BasicForwardRuleInfGraph;
import ie.tcd.cs.nembes.coror.reasoner.rulesys.Rule;
import ie.tcd.cs.nembes.coror.reasoner.rulesys.impl.TemporalFRuleEngineI;
import ie.tcd.cs.nembes.coror.util.ArrayList;
import ie.tcd.cs.nembes.coror.util.ArraySet;
import ie.tcd.cs.nembes.coror.util.Iterator;
import ie.tcd.cs.nembes.coror.util.iterator.ExtendedIterator;

public class LEAPSEngine implements TemporalFRuleEngineI {
	
	protected LeapsRuleBase ruleBase;
    protected static LeapsWorkingMemory memory;
    protected LeapsRuleSet ruleSet;

	  /** The parent InfGraph which is employing this engine instance */
    protected static BasicForwardRuleInfGraph infGraph;
    
    
    /** SetImpl of rules being used */
    protected ArrayList rules;

    /** Queue of newly added triples waiting to be processed */
    protected ArrayList addsPending = new ArrayList();
    
    /** Queue of newly deleted triples waiting to be processed */
    protected ArrayList deletesPending = new ArrayList();
    
    /** List of predicates used in rules to assist in fast data loading */
    protected ArraySet predicatesUsed;
    
    /** Flag, if true then there is a wildcard predicate in the rule set so that selective insert is not useful */
    protected static boolean wildcardRule;
    
    /** performance stats - number of rules fired */
    long nRulesFired = 0;
 
    /**
     * Constructor.
     * @param parent the F or FB infGraph that it using this engine, the parent graph
     * holds the deductions graph and source data.
     * @param rules the rule set to be processed
     */
    public LEAPSEngine(BasicForwardRuleInfGraph parent, ArrayList rules) {
        infGraph = parent;
        this.rules = rules;
        ruleBase = new LeapsRuleBase();
        memory = ruleBase.newWorkingMemory();
        ruleSet = ruleBase.newNewLeapsRuleSet("manners");
    }
    /**
     * Constructor. Build an empty engine to which rules must be added
     * using setRuleStore().
     * @param parent the F or FB infGraph that it using this engine, the parent graph
     * holds the deductions graph and source data.
     */
    public LEAPSEngine(BasicForwardRuleInfGraph parent) {
        infGraph = parent;
        ruleBase = new LeapsRuleBase();
        memory = ruleBase.newWorkingMemory();
        ruleSet = ruleBase.newNewLeapsRuleSet("manners");
    }
    
    /**
     * Process all available data. This should be called once a deductions graph
     * has be prepared and loaded with any precomputed deductions. It will process
     * the rule axioms and all relevant existing exiting data entries.
     * @param ignoreBrules set to true if rules written in backward notation should be ignored
     * @param inserts the set of triples to be processed, normally this is the
     * raw data graph but may include additional deductions made by preprocessing hooks
     */
    @Override
    public void init(boolean ignoreBrules, Finder inserts) {
            compile(rules, ignoreBrules);
            fastInit(inserts);
            try {
				memory.fireAllRules();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    
    }
    /**
     * Process all available data..
     * @param inserts the set of triples to be processed, normally this is the
     * raw data graph but may include additional deductions made by preprocessing hooks
     */
    @Override
    public void fastInit(Finder inserts) {

        if (infGraph.getRawGraph() != null) {
            if (wildcardRule) {
                for (Iterator i = inserts.find(new TriplePattern(null, null, null)); i.hasNext(); ) {
                    addTriple((Triple)i.next(), false);
                }
            } else {
                // only insert the data matches the predicates used in the rules
                for (Iterator p = predicatesUsed.iterator(); p.hasNext(); ) {
                    Node predicate = (Node)p.next();
                    for (Iterator i = inserts.find(new TriplePattern(null, predicate, null)); i.hasNext(); ) {
                        Triple t = (Triple)i.next();
                        addTriple(t,false);
                        
                    }
                }
            }
        }
    }

	@Override
	public void add(Triple t) {
		// TODO Auto-generated method stub
		
	}
	public static void addTriple(Triple t, boolean deduction) {
      if (deduction) {
    	  if ( !contains(t)){
    	  	//System.out.println("ADDING DEDUCTION: "+t);
            infGraph.addDeduction(t);
    	  }
    	  else{return;}
       }
      //System.out.println("ADDING TRIP: "+t);
      memory.assertObject(t);
		
	}

    public static boolean  contains(Triple t) {
        return contains(t.getSubject(), t.getPredicate(), t.getObject());
    }
    
    /**
     * Return true if the triple pattern is already in either the graph or the stack.
     * I.e. it has already been deduced.
     */
    public static boolean contains(Node s, Node p, Node o) {
        ExtendedIterator it = find(s, p, o);
        boolean result = it.hasNext();
        it.close();
        return result;
    }
    
    /**
     * In some formulations the context includes deductions that are not yet
     * visible to the underlying graph but need to be checked for.
     * However, currently this calls the graph find directly.
     */
    public static ExtendedIterator find(Node s, Node p, Node o) {
        return infGraph.find(s, p, o);
    }
    
	@Override
	public boolean delete(Triple t) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void sweepRETE(long start, long end) {
		// TODO Auto-generated method stub
		
	}
	
	 /**
     * Compile a list of rules into the internal rule store representation.
     * @param rules the list of Rule objects
     * @param ignoreBrules set to true if rules written in backward notation should be ignored
     */
    public void compile(ArrayList rules, boolean ignoreBrules) {
       
        predicatesUsed = new ArraySet();
        wildcardRule = false;
            
        for (Iterator it = rules.iterator(); it.hasNext(); ) {
            CororLeapsRule rule = (CororLeapsRule)it.next();
            if (ignoreBrules && rule.isBackward()) continue;
            
            try {
				rule.convertToLeapsFormat(predicatesUsed);
				ruleSet.addRule(rule);
			} catch (LeapsDuplicateRuleNameException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (LeapsInvalidRuleException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
 
        }
        if (wildcardRule) predicatesUsed = null;
    }
            
          

}
