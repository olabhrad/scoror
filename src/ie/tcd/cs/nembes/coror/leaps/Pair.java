package ie.tcd.cs.nembes.coror.leaps;

public class Pair {

	int ceIndex; //Condition Element index in the Rule
	int atrributeIndex; // 0,1,2 = Subject,Predicate,Object
	
	public Pair(int ceIndex, int atrributeIndex){
		this.ceIndex=ceIndex;
		this.atrributeIndex = atrributeIndex;
	}
}
