/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ie.tcd.cs.nembes.coror.reasoner.rulesys.builtins;

/**
 * A head biultin called only when a inconsistency is detected
 * @author WEI TAI
 */
public class False extends BaseBuiltin{

    
    
    @Override
    public String getName() {
        return "false";
    }
    
    public void headAction(){
        System.err.println("Inconsistency detected");
    }
    
}
