/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ie.tcd.cs.nembes.coror.reasoner.rulesys.builtins;

import ie.tcd.cs.nembes.coror.graph.Node;
import ie.tcd.cs.nembes.coror.graph.Triple;
import ie.tcd.cs.nembes.coror.reasoner.rulesys.RuleContext;
import ie.tcd.cs.nembes.coror.reasoner.rulesys.Util;
import ie.tcd.cs.nembes.coror.util.ArrayList;

/**
 * Accepts a list and map the elements pairwise in a triple. This builtin is 
 * specially designed for list processing in OWL 2.
 * 
 * @author WEI TAI
 */
public class ListMapAsPair extends BaseBuiltin{

    @Override
    public String getName() {
        return "listMapAsPair";
    }
    
    public int getArgLength(){
        return 3;
    }
    
    @Override
    public void headAction(Node[] args, int length, RuleContext context){
        
        checkArgs(length, context);
        Node l = getArg(0, args, context);
        Node in = getArg(1, args, context);
        Node c = getArg(2, args, context);
        
        if(!Util.isNumeric(l)) return;
        
        int index = Util.getIntValue(in);
        
        ArrayList list = Util.convertList(l, context);
        
        for(int i=0; i<list.size(); i++){
            for(int j=i+1; j<list.size(); j++){
                switch(index){
                    case 0: 
                        context.add(new Triple(c, (Node)list.get(i), (Node)list.get(j)));
                        break;
                    case 1: 
                        context.add(new Triple((Node)list.get(i), c, (Node)list.get(j)));
                        break;
                    case 2: 
                        context.add(new Triple((Node)list.get(i), (Node)list.get(j), c));
                        break;
                    default: 
                        return;
                }
            }
        }
    }
    
}
