package ie.tcd.cs.nembes.coror.reasoner.rulesys.impl;

import ie.tcd.cs.nembes.coror.graph.Triple;
import ie.tcd.cs.nembes.coror.graph.enh.TemporalTriple;
import ie.tcd.cs.nembes.coror.reasoner.TriplePattern;
import ie.tcd.cs.nembes.coror.reasoner.rulesys.*;
import ie.tcd.cs.nembes.coror.shared.CororException;
//import ie.tcd.cs.nembes.microjenaenh.reasoner.rulesys.enh.TempTriple;
import ie.tcd.cs.nembes.coror.util.Iterator;
import ie.tcd.cs.nembes.coror.util.ArrayList;

/**
 * Manages a set of ready-to-fire rules. For monotonic rule sets
 * we simply fire the rules as soon as they are triggered. For non-monotonic
 * rule sets we stack them up in a conflict set and fire them one-at-a-time,
 * propagating all changes between times.
 * <p>
 * Note, implementation is not thread-safe. Would be easy to make it so but 
 * concurrent adds to InfModel are not supported anyway.
 * 
 * @author <a href="mailto:der@hplb.hpl.hp.com">Dave Reynolds</a>
 * @version $Revision: 1.6 $
 */

public class RETEConflictSet {

	// Comments by Wei
	// All loggers are removed
//	protected static Log logger = LogFactory.getLog(FRuleEngine.class);

    /** the execution context for this conflict set */
    protected RETERuleContext gcontext;

    /** false if the overall rule system contains some non-montotonic rules */
    protected boolean isMonotonic;
    
    /** the list of rule activations left to fire */
    protected ArrayList conflictSet = new ArrayList();

    /** count the number of positive entries - optimization hack */
    protected int nPos = 0;
    
    /** count the number of negative entries - optimization hack */
    protected int nNeg = 0;
    
    /** Construct an empty conflict set, noting whether the overall rule system is monotonic or not */
    public RETEConflictSet(RETERuleContext context, boolean isMonotonic) {
        this.gcontext = context;
        this.isMonotonic = isMonotonic;
    }
    
    /**
     * Record a request for a rule firing. For monotonic rulesets it may be
     * actioned immediately, otherwise it will be stacked up.
     */
    public void add(Rule rule, BindingEnvironment env, boolean isAdd) {
        if (isMonotonic) {
            RETERuleContext context = new RETERuleContext((BasicForwardRuleInfGraph)gcontext.getGraph(), gcontext.getEngine());
            context.setEnv(env);
            context.setRule(rule);
            execute(context, isAdd);
        } else {
            // Add to the conflict set, compressing +/- pairs
            boolean done = false;
            if ( (isAdd && nNeg > 0) || (!isAdd && nPos > 0) ) {
                for (Iterator i = conflictSet.iterator(); i.hasNext(); ) {
                    CSEntry cse = (CSEntry)i.next();
                    if (cse.rule != rule) continue;
                    if (cse.env.equals(env)) {
                        if (isAdd != cse.isAdd) {
                            try {
                                    i.remove();
                            } catch (CororException e) {
                                    e.printStackTrace();
                            }
						
                            if (cse.isAdd) nPos--; else nNeg --;
                            done = true;
                        } else {
                            // Redundant insert? Probably leave in for side-effect cases like print
                        }
                    }
                }
            }
            if (!done) {
                conflictSet.add(new CSEntry(rule, env, isAdd));
                if (isAdd) nPos++; else nNeg++;
            }
        }
    }

    /**
     * Return true if there are no more rules awaiting firing.
     */
    public boolean isEmpty() {
        return conflictSet.isEmpty();
    }
    
    /**
     * Pick on pending rule from the conflict set and fire it.
     * Return true if there was a rule to fire.
     */
    public boolean fireOne() {
        if (isEmpty()) return false;
        int index = conflictSet.size() - 1;
        CSEntry cse = (CSEntry)conflictSet.remove(index);
        if (cse.isAdd) nPos--; else nNeg --;
        RETERuleContext context = new RETERuleContext((BasicForwardRuleInfGraph)gcontext.getGraph(), gcontext.getEngine());
        context.setEnv(cse.env);
        context.setRule(cse.rule);
        if (context.shouldStillFire()) {
            execute(context, cse.isAdd);
        }
        
        return true; 
    }
    
    /**
     * Execute a single rule firing. 
     */
    public static void execute(RETERuleContext context, boolean isAdd) {

        Rule rule = context.getRule();
        BindingEnvironment env = context.getEnv();
        BasicForwardRuleInfGraph infGraph = (BasicForwardRuleInfGraph)context.getGraph();
        
        RETEEngine engine = context.getEngine();
        engine.incRuleCount();

        for (int i = 0; i < rule.headLength(); i++) {
            Object hClause = rule.getHeadElement(i);
            if (hClause instanceof TriplePattern) {
                Triple t = env.instantiate((TriplePattern) hClause);
                if(env instanceof BindingVectorTemporal){
                    t = new TemporalTriple(t.getSubject(), t.getPredicate(), t.getObject(), ((BindingVectorTemporal)env).getTime());
                }

//                if (!t.getSubject().isLiteral()) {
//                    if(ReasonerConfig.truthMaintenance){
//                        if(isAdd) engine.addDeductionTriple(t);
//                        else engine.deleteDeductionTriple(t);
//                    }
//                    else{
                        // Only add the result if it is legal at the RDF level.
                        // E.g. RDFS rules can create assertions about literals
                        // that we can't record in RDF
                        if (isAdd) {
//                            System.err.println("check point in RETEConflictSet::execute (" + t+")");
                            if ( ! context.contains(t)) {
//                                System.err.println("    not contain triple");
//                                                System.err.println(context.rule.getName() + " --> " + t);
                                engine.addTriple(t, true);
                                if(ReasonerConfig.trackTrace){
                                    ArrayList matchList = new ArrayList(rule.bodyLength());
                                    for (int j = 0; j < rule.bodyLength(); j++) {
                                        Object clause = rule.getBodyElement(j);
                                        if (clause instanceof TriplePattern) {
                                            matchList.add(env.instantiate((TriplePattern)clause));
                                        } 
                                    }
                                }                                
                            }
                        } else {
                            if ( context.contains(t)) {
                                // Remove the generated triple
                                engine.deleteTriple(t, true);
                            }
                        }
//                    }
//                }
            } else if (hClause instanceof Functor && isAdd) {
//                Functor f = (Functor)hClause;
//                Builtin imp = f.getImplementor();
//                if (imp != null) {
//                    imp.headAction(f.getBoundArgs(env), f.getArgLength(), context);
//                } else {
//                    throw new CororException("RETEConflictSet::execute -- Invoking undefined Functor " + f.getName() +" in " + rule.toShortString());
//                }
                throw new CororException("functor is not supported in rule head");
            } else if (hClause instanceof Rule) {
//                Rule r = (Rule)hClause;
//                if (r.isBackward()) {
//                    if (isAdd) {
//                        infGraph.addBRule(r.instantiate(env));
//                    } else {
//                        infGraph.deleteBRule(r.instantiate(env));
//                    }
//                } else {
//                    throw new CororException(" Found non-backward subrule : " + r); 
//                }
                throw new CororException("backward rule is not supported in rule head");
            }
        }        
    }
        
    // Inner class representing a conflict set entry 
    private static class CSEntry {
        protected Rule rule;
        protected BindingEnvironment env;
        protected boolean isAdd;
        
        CSEntry(Rule rule, BindingEnvironment env, boolean isAdd) {
            this.rule = rule;
            this.env = env;
            this.isAdd = isAdd;
        }
    }
}
