package ie.tcd.cs.nembes.coror.reasoner.rulesys.impl;

import ie.tcd.cs.nembes.coror.graph.Graph;
import ie.tcd.cs.nembes.coror.graph.Node;
import ie.tcd.cs.nembes.coror.graph.Triple;
import ie.tcd.cs.nembes.coror.reasoner.Finder;
import ie.tcd.cs.nembes.coror.reasoner.TriplePattern;
import ie.tcd.cs.nembes.coror.reasoner.rulesys.*;
import ie.tcd.cs.nembes.coror.shared.CororException;
import ie.tcd.cs.nembes.coror.util.*;
import ie.tcd.cs.nembes.coror.util.ext.AbstractCollection;
import ie.tcd.cs.nembes.coror.util.iterator.ConcatenatedIterator;




/**
 * A RETE version of the the forward rule system engine. It needs to reference
 * an enclosing ForwardInfGraphI which holds the raw data and deductions.
 * 
 * @author <a href="mailto:der@hplb.hpl.hp.com">Dave Reynolds</a>
 * @version $Revision: 1.30 $ on $Date: 2008/01/02 12:06:16 $
 */
public class RETEEngine implements TemporalFRuleEngineI {
    
    /** The parent InfGraph which is employing this engine instance */
    protected BasicForwardRuleInfGraph infGraph;
    
    
    /** SetImpl of rules being used */
    protected ArrayList rules;
    
    /** From predicate node to clause, Node_ANY is used for wildcard predicates */
    protected MultiArrayMap clauseIndex;
    
    /** Queue of newly added triples waiting to be processed */
    protected ArrayList addsPending = new ArrayList();
    
    /** Queue of newly deleted triples waiting to be processed */
    protected ArrayList deletesPending = new ArrayList();
    
    /** The conflict set of rules waiting to fire */
    protected RETEConflictSet conflictSet;
    
    /** List of predicates used in rules to assist in fast data loading */
    protected ArraySet predicatesUsed;
    
    /** Flag, if true then there is a wildcard predicate in the rule set so that selective insert is not useful */
    protected boolean wildcardRule;
    
    /** performance stats - number of rules fired */
    long nRulesFired = 0;
    
    /** True if we have processed the axioms in the rule set */
    boolean processedAxioms = false;
    
    /** 
     * True if all the rules are monotonic, so we short circuit the conflict set processing.
     * The functors (builtins) contained in the head of a rule determine whether
     * that rule is monotonic. If non-monotonic functors such as noValue or 
     * remove are used then the rule is not monotonic and so it is with this engine.
     */
    boolean isMonotonic = true;
    
//  =======================================================================
//  Fields for new shared RETE
//  =======================================================================  
    
    /** key is rule id and each entry of a value list is a variable list for a clause/alpha node */
    private MultiArrayMap ruleConditionVarList;
    
    /** rule with a list of conditions */
    protected MultiArrayMap ruleConditions = new MultiArrayMap();
   
    
    
    /**
     * Constructor.
     * @param parent the F or FB infGraph that it using this engine, the parent graph
     * holds the deductions graph and source data.
     * @param rules the rule set to be processed
     */
    public RETEEngine(BasicForwardRuleInfGraph parent, ArrayList rules) {
        infGraph = parent;
        this.rules = rules;
        // Check if this is a monotonic rule set
        isMonotonic = true;
        
        // Java iterator been replaced by microJena Iterator
        for (Iterator i = rules.iterator(); i.hasNext(); ) {
            Rule r = (Rule)i.next();

            if ( ! r.isMonotonic() ) {
                isMonotonic = false;
                break;
            }
        }
    }

    /**
     * Constructor. Build an empty engine to which rules must be added
     * using setRuleStore().
     * @param parent the F or FB infGraph that it using this engine, the parent graph
     * holds the deductions graph and source data.
     */
    public RETEEngine(BasicForwardRuleInfGraph parent) {
        infGraph = parent;
    }
    
    
//  =======================================================================
//  Control methods

    /**
     * Process all available data. This should be called once a deductions graph
     * has be prepared and loaded with any precomputed deductions. It will process
     * the rule axioms and all relevant existing exiting data entries.
     * @param ignoreBrules set to true if rules written in backward notation should be ignored
     * @param inserts the set of triples to be processed, normally this is the
     * raw data graph but may include additional deductions made by preprocessing hooks
     */
    public void init(boolean ignoreBrules, Finder inserts) {
    
        if(ReasonerConfig.shareNodes == true){
            // RETE with shared nodes
            compileAlpha(rules, ignoreBrules);
            compileBeta();
            findAndProcessAxioms();
            fastInit(inserts);
            runAll();
        }
        else {
            // normal RETE
            compile(rules, ignoreBrules);
            findAndProcessAxioms();
            fastInit(inserts);
            runAll();
        }
    }
    
    /**
     * Process all available data. This version expects that all the axioms 
     * have already be preprocessed and the clause index already exists.
     * @param inserts the set of triples to be processed, normally this is the
     * raw data graph but may include additional deductions made by preprocessing hooks
     */
    public void fastInit(Finder inserts) {
        conflictSet = new RETEConflictSet(new RETERuleContext(infGraph, this), isMonotonic);

//        findAndProcessActions();
        if (infGraph.getRawGraph() != null) {
            // Insert the data
            if (wildcardRule) {
                for (Iterator i = inserts.find(new TriplePattern(null, null, null)); i.hasNext(); ) {
                    addTriple((Triple)i.next(), false);
                }
            } else {
                // only insert the data matches the predicates used in the rules
                for (Iterator p = predicatesUsed.iterator(); p.hasNext(); ) {
                    Node predicate = (Node)p.next();
                    for (Iterator i = inserts.find(new TriplePattern(null, predicate, null)); i.hasNext(); ) {
                        Triple t = (Triple)i.next();
                        addTriple(t, false);
                    }
                }
            }
        }
    }
    
    
    
    /**
     * Add one triple to the data graph, run any rules triggered by
     * the new data item, recursively adding any generated triples.
     */
    public synchronized void add(Triple t) {
        addTriple(t, false);
        runAll();
    }
    
    /**
     * Remove one triple to the data graph.
     * @return true if the effects could be correctly propagated or
     * false if not (in which case the entire engine should be restarted).
     */
    public synchronized boolean delete(Triple t) {
        deleteTriple(t, false);
        runAll();
        return true;
    }
    
//    /**
//     * Return the number of rules fired since this rule engine instance
//     * was created and initialized
//     */
//    public long getNRulesFired() {
//        return nRulesFired;
//    }
    
//    /**
//     * Access the precomputed internal rule form. Used when precomputing the
//     * internal axiom closures.
//     */
//    public Object getRuleStore() {
//        return new RuleStore(clauseIndex, predicatesUsed, wildcardRule, isMonotonic);
//    }
    
//    /**
//     * SetImpl the internal rule from from a precomputed state.
//     */
//    public void setRuleStore(Object ruleStore) {
//        RuleStore rs = (RuleStore)ruleStore;
//        predicatesUsed = rs.predicatesUsed;
//        wildcardRule = rs.wildcardRule;
//        isMonotonic = rs.isMonotonic;
//        
//        // Clone the RETE network to this engine
//        RETERuleContext context = new RETERuleContext(infGraph, this);
//        MapImpl netCopy = new MapImpl();
//        clauseIndex = new MultiArrayMap();
//        for (Iterator i = rs.clauseIndex.entrySet().iterator(); i.hasNext(); ) {
//            MapImpl.Entry entry = (MapImpl.Entry)i.next();
//            clauseIndex.put(entry.getKey(), ((RETENode)entry.getValue()).clone(netCopy, context));
//        }
//    }
    
    /**
     * Add a rule firing request to the conflict set.
     */
    public void requestRuleFiring(Rule rule, BindingEnvironment env, boolean isAdd) {
        conflictSet.add(rule, env, isAdd);
    }
    
    /**
     * Compile alpha network for the loaded rules. Unlike the original 
     * construction method where alpha network and beta network are constructed
     * in one go. Here we separate the construction of alpha network and that of#
     * beta network in order to fit in optimizations.
     */
    void compileAlpha(ArrayList rules, boolean ignoreBrules) {
        
        if(clauseIndex == null) clauseIndex = new MultiArrayMap();
        if(ruleConditionVarList == null) ruleConditionVarList = new MultiArrayMap();
        predicatesUsed = new ArraySet();
        wildcardRule = false;
        
        for (Iterator it = rules.iterator(); it.hasNext(); ) {
            
            Rule rule = (Rule)it.next();
                    
            // conditionLength is to record how many conditions are contained by
            // this rule. If only one, then attach it a RETETerminal instead
            // of a RETEQueue.
            
            int bodyLength = rule.bodyLength();
            for (int i = 0; i < bodyLength; i++) {

                Object clause = rule.getBodyElement(i);
                
                // an indicator for whether the clause share alpha node with the others.
                boolean isShared = false;
                // Create the filter node for this pattern
                if (clause instanceof TriplePattern) {
                    
                    TriplePattern pattern = (TriplePattern)clause;
                    
                    // get the number of variables in the pattern
                    int numVar = 0;
                    Node subject = pattern.getSubject();
                    Node predicate = pattern.getPredicate();
                    Node object = pattern.getObject();
                    if(subject.isVariable()) numVar ++;
                    if(predicate.isVariable()) numVar ++;
                    if(object.isVariable()) numVar++;
                    
                    // generate a variable list for the pattern. The variable list will be used later for generating join strategies
                    Node_RuleVariable[] varList = new Node_RuleVariable[numVar];
                    int nextPos = 0;
                    if(subject.isVariable()) varList[nextPos++] = (Node_RuleVariable)subject;
                    if(predicate.isVariable()) varList[nextPos++] = (Node_RuleVariable)predicate;
                    if(object.isVariable()) varList[nextPos++] = (Node_RuleVariable)object;
                    
                    RETEClauseFilterNS clauseNode = compileOrShare(pattern, numVar, generateFilterNodeName(rule,i));                                                                        
                    ruleConditions.put(rule, clauseNode);
                    ruleConditionVarList.put(rule, varList);
                    
                    if (predicate.isVariable()) {
                        addToClauseIndex(Node.ANY, clauseNode);
                        wildcardRule = true;                        
                    } else {
                        addToClauseIndex(predicate, clauseNode);
    
                        if (! wildcardRule) {
                            predicatesUsed.add(predicate);
                        }
                    }
                }
                else{
                    // non-triple pattern clause, e.g. function, built-ins
                }
            }                              
        }
         
        // dont understand why need predicatesUsed: it is used to selectively match triples with used predicates
        if (wildcardRule) predicatesUsed = null;        
    }
    
    /**
     * Return an existing RETEClauseFilterNS node if it shares the pattern
     * with the input TriplePattern or otherwise construct a new TriplePattern.
     */
    RETEClauseFilterNS compileOrShare(TriplePattern clause, int envLength, String name){
        
        AbstractCollection filterNodeSet = clauseIndex.values();
        RETEClauseFilterNS currentNode;
        Iterator filterNodeIt = filterNodeSet.iterator();
        
        while(filterNodeIt.hasNext()){
            currentNode = (RETEClauseFilterNS) filterNodeIt.next();
            if(currentNode.matchPattern(clause)) return currentNode;
        }
//        System.err.println(name+" is not shared with anyone");
        
        return RETEClauseFilterNS.compile(clause, envLength, name);
    }
    
    /**
     * Add a condition to the clauseIndex only when it is not already existent.
     */
    private void addToClauseIndex(Node node, RETEClauseFilterNS clause){
        Iterator i = clauseIndex.getAll(node);
        while(i.hasNext()){
            if(i.next() == clause)
                return;
        }
        clauseIndex.put(node, clause);
    }
    
    /**
     * Generate an ID for an alpha node
     * @return 
     */
    private String generateFilterNodeName(Rule rule, int index){
        return rule.getName()+"."+String.valueOf(index);
    }
    
    /**
     * Generate an ID for a beta node
     */
    private String generateQueueNodeName(String left, String right){
        return "("+left+"+"+right+")";
    }
//    
//    private void cloneBooleanArray( boolean[] backupSeenVar, boolean[] seenVar){
//        for(byte i=0; i<seenVar.length; i++){
//            backupSeenVar[i] = seenVar[i];
//        }
//    }
    
    /**
     * Compile a shared beta network. A beta node can be shared only 
     */
    private void compileBeta(){

        Iterator ruleIt = ruleConditions.keySet().iterator();
        
        while(ruleIt.hasNext()){
            
            Object rule = ruleIt.next();
            Iterator conditionIt = ruleConditions.getAll(rule);
            Iterator varListIt = ruleConditionVarList.getAll(rule);
            SharedNodeI prior = null;
            Node_RuleVariable[] priorVarList = null;
//            if(CororReasoner.printTestInfo) {
//                System.err.println();
//                System.err.println("==========="+((Rule)rule).getName()+"===========");
//            }

//            System.err.println("Compiling beta network for rule " + ((Rule)rule).getName() + " ...");
            while(conditionIt.hasNext() && varListIt.hasNext()){
                RETEClauseFilterNS condition = (RETEClauseFilterNS)conditionIt.next();
                Node_RuleVariable[] varList = (Node_RuleVariable[])varListIt.next();
                
//                if(CororReasoner.printTestInfo) {
//                    System.err.print("Filter Node "+condition.getNodeID()+" varList ");
//                    for(int i=0; i<varList.size(); i++)
//                        System.err.print(((Node_RuleVariable)varList.get(i)).getIndex()+" ");
//                    System.err.println();
//                }
                
                if(prior == null ) {
                    prior = condition;
                    priorVarList = varList;
                    continue;
                }                

                /**
                 * Calculate join strategy. The size of varList is exactly the same
                 * as the number of variables in the corresponding RETEClauseFilterNS.
                 */
                JoinStrategy strategy = new JoinStrategy();
                for(int i=0; i<priorVarList.length; i++){
                    
                    Node_RuleVariable priorVar = (Node_RuleVariable)priorVarList[i];
                    Node_RuleVariable currentVar;
                    for(int j=0; j<varList.length; j++){
                        currentVar = (Node_RuleVariable)varList[j];
                        if(Node_RuleVariable.sameNodeAs(priorVar, currentVar)){
                            strategy.putStrategy((byte)i, (byte)j);
                        }                        
                    }
                }    
                
                // calculate the new priorVarList
                Node_RuleVariable[] newPriorVarList = new Node_RuleVariable[priorVarList.length+varList.length-strategy.count];
                int nextPos = priorVarList.length;
                System.arraycopy(priorVarList, 0, newPriorVarList, 0, priorVarList.length);
                for(int i=0; i<varList.length;i++){
                    boolean exist = false;
                    for(int j=0; j<priorVarList.length; j++){
                        if(Node_RuleVariable.sameNodeAs((Node_RuleVariable)varList[i], (Node_RuleVariable)priorVarList[j])){
                            exist = true;
                            break;
                        }
                    }
                    if(!exist) newPriorVarList[nextPos++] = varList[i];
                }
                

                /**
                 * Construct shared RETEQueueNS. There are two level of sharing.
                 * ShareQueue is for those RETEQueueNS nodes serving as continuation 
                 * for the same RETEClauseFilterNS node. All RETEQueueNS continuations 
                 * of a RETEClauseFilterNS share a queue. 
                 * ShareJoin is a higher level of sharing and is based on sharing queue. 
                 * ShareJoin comes into effect only when both the left input and the 
                 * right input are shared with another rule. And the join sequences 
                 * are the same. 
                 */
                boolean shareJoin = false;
                boolean shareQueue = false;
                
                ArrayMap sharedQueue = null;
                RETEQueueNS leftQ = null;
                ArrayList priorContList = prior.getContinuations();
                for(int i=0; i<priorContList.size(); i++){
                    /**
                     * A continuation can be either RETETerminal or RETEQueueNS.
                     * For sharing we only consider RETEQueueNS.
                     */
                    if(priorContList.get(i) instanceof RETEQueueNS){
                        RETEQueueNS queueCont = (RETEQueueNS)priorContList.get(i);
                        sharedQueue = queueCont.queue;
                        shareQueue = true;
                        
                        /**
                         * Check for share join.
                         * The shared RETEQueueNS must have the same parents (I mean, exactly the same) to share join.
                         * Now we already know that the leftQ can be shared. We only need to confirm that the rightQ can be shared
                         * and the join strategy is the same.
                         */
                        if(queueCont.sibling.containParent(condition) && 
                                queueCont.getJoinStrategies().sameStrategyAs(strategy)){
                            /** 
                             * Prior(always leftQ or filter node) already has continuations. 
                             * The sibling's parent of this continuation (must be a Filter Node)
                             * is the same as this condition. The join strategy are the same.
                             * Then share
                             */
                            leftQ = queueCont;
                            shareJoin = true;
                            break;
                        }
                        break;
                    }
                }
                if(shareQueue == false)
                    leftQ = new RETEQueueNS(strategy, true, newPriorVarList.length, generateQueueNodeName(prior.getNodeID(), condition.getNodeID())); // now the priorVarList is the newPriorVarList
                else
                    leftQ = new RETEQueueNS(strategy, sharedQueue, true, newPriorVarList.length, generateQueueNodeName(prior.getNodeID(), condition.getNodeID()));
                
                RETEQueueNS rightQ = null;
                if(shareJoin)
                    rightQ = leftQ.sibling;
                else{
                    ArrayList continuations = condition.getContinuations();
                    for(int i=0; i<continuations.size(); i++)
                        if(continuations.get(i) instanceof RETEQueueNS){
                            // construct a rightQ with shared buffer
                            rightQ = new RETEQueueNS(strategy, ((RETEQueueNS)continuations.get(i)).queue, false, newPriorVarList.length, generateQueueNodeName(prior.getNodeID(), condition.getNodeID()));
                            break;
                        }
                    if(rightQ == null) // construct a rightQ with new buffer
                        rightQ = new RETEQueueNS(strategy, false, newPriorVarList.length, generateQueueNodeName(prior.getNodeID(), condition.getNodeID()));  
                }
                
//                if(CororReasoner.printTestInfo) {
//                    System.err.println("    join node "+leftQ.getNodeID());
//                    System.err.print("    join strategy ");
//                    for(int i=0; i<strategy.left.length; i++){
//                        if(strategy.left[i] == -1) break;
//                        System.err.print(strategy.left[i]+"-"+strategy.right[i]+" ");
//                    }
//                    System.err.println();
//                    System.err.print("    join node output varList ");
//                    for(int i=0; i<priorVarList.size(); i++)
//                        System.err.print(((Node_RuleVariable)priorVarList.get(i)).getIndex()+" ");
//                    System.err.println();
//                }
                                
                if(!shareJoin){
                    leftQ.setSibling(rightQ);
                    rightQ.setSibling(leftQ);
                    prior.addContinuation(leftQ);
                    if(prior instanceof RETEQueueNS) ((RETEQueueNS)prior).sibling.addContinuation(leftQ);
                    condition.addContinuation(rightQ);
                }
                prior = leftQ; 
                priorVarList = newPriorVarList;
                
//                System.err.println(leftQ.getNodeID());
//                System.err.println(strategy);
//                System.err.println("newPBV size is "+newPriorVarList.length);
            }
            if ( prior != null ) {
                RETETerminal terminal = new RETETerminal((Rule)rule, this, infGraph);
                prior.addContinuation(terminal);
//                System.err.println("installing rule head "+ruleId+(RETETerminal)ruleTerminalMap.get(ruleId));
            }
        }
        System.err.println(getRETEAsGraph());
    }
    
    String getRETEAsGraph(){
        String str = "";
        Iterator ruleIt = ruleConditions.keySet().iterator();
        while(ruleIt.hasNext()){
            Rule rule = (Rule) ruleIt.next();
            str += rule.getName()+" : ";
            Iterator conditionIt = ruleConditions.getAll(rule);
            String joinStr = "";
            boolean firstCondition = true;
            while(conditionIt.hasNext()){
                RETEClauseFilterNS condition = (RETEClauseFilterNS) conditionIt.next();
                if(firstCondition) {
                    joinStr += condition.getNodeID();
                    firstCondition = false;
                    continue;
                }
                joinStr = "( " + joinStr + " + " + condition.getNodeID() + " )";
            }
            str += joinStr + " \n";
            
        }
        return str;
    }
    
    
    /**
     * Compile a list of rules into the internal rule store representation.
     * @param rules the list of Rule objects
     * @param ignoreBrules set to true if rules written in backward notation should be ignored
     */
    public void compile(ArrayList rules, boolean ignoreBrules) {
        
        clauseIndex = new MultiArrayMap();
        predicatesUsed = new ArraySet();
        wildcardRule = false;
            
        for (Iterator it = rules.iterator(); it.hasNext(); ) {
            Rule rule = (Rule)it.next();
            if (ignoreBrules && rule.isBackward()) continue;
            
            int numVars = rule.getNumVars();
            boolean[] seenVar = new boolean[numVars];
            RETESourceNode prior = null;
            
            for (int i = 0; i < rule.bodyLength(); i++) {
                Object clause = rule.getBodyElement(i);
           
                if (clause instanceof TriplePattern) {

                    // Create the filter node for this pattern
                	// Comment by Wei:
                	// ArrayList is replaced by Vector
                    ArrayList clauseVars = new ArrayList(numVars);
                    RETEClauseFilter clauseNode = RETEClauseFilter.compile((TriplePattern)clause, numVars, clauseVars);
                    Node predicate = ((TriplePattern)clause).getPredicate();
            
                    if (predicate.isVariable()) {
                        clauseIndex.put(Node.ANY, clauseNode);
                        wildcardRule = true;
                    } else {
                        clauseIndex.put(predicate, clauseNode);
                        if (! wildcardRule) {
                            predicatesUsed.add(predicate);
                        }
                    }
                
                    // Create list of variables which should be cross matched between the earlier clauses and this one
                    ArrayList matchIndices = new ArrayList(numVars);
                    for (Iterator iv = clauseVars.iterator(); iv.hasNext(); ) {
                        int varIndex = ((Node_RuleVariable)iv.next()).getIndex();
                        if (seenVar[varIndex]) matchIndices.add(new Byte((byte)varIndex));
                        seenVar[varIndex] = true;
                    }
                    
                    // Build the join node
                    if (prior == null) {
                        // First clause, no joins yet
                        prior = clauseNode;
                    } else {
                        RETEQueue leftQ = new RETEQueue(matchIndices);
                        RETEQueue rightQ = new RETEQueue(matchIndices);
                        leftQ.setSibling(rightQ);
                        rightQ.setSibling(leftQ);
                        clauseNode.setContinuation(rightQ);
                        prior.setContinuation(leftQ);
                        prior = leftQ;
                    }
                }
            }
            
            // Finished compiling a rule - add terminal 
            if (prior != null) {
                RETETerminal term = new RETETerminal(rule, this, infGraph);
                prior.setContinuation(term);
            }
                    
        }
            
        if (wildcardRule) predicatesUsed = null;
    }    

//    /**
//     * Create a terminal node for the RETE network. Normally this is RETETerminal
//     * but some people have experimented with alternative delete handling by
//     * creating RETETerminal subclasses so this hook simplifies use of that
//     * approach.
//     */
//    protected RETETerminal createTerminal(Rule rule) {
//        return new RETETerminal(rule, this, infGraph);
//    }
    
//  =======================================================================
//  Internal implementation methods

    /**
     * Add a new triple to the network. 
     * @param triple the new triple
     * @param deduction true if the triple has been generated by the rules and so should be 
     * added to the deductions graph.
     */
    public synchronized void addTriple(Triple triple, boolean deduction) {
        if (deletesPending.size() > 0) deletesPending.remove(triple);
        addsPending.add(triple);
        if (deduction) {
            infGraph.addDeduction(triple);
        }
    }
    
    /**
     * Remove a new triple from the network. 
     * @param triple the new triple
     * @param deduction true if the remove has been generated by the rules 
     */
    public synchronized void deleteTriple(Triple triple, boolean deduction) {
        
//        boolean delete = true;
        addsPending.remove(triple);
        deletesPending.add(triple);
        if (deduction) {
            infGraph.getDeductionsGraph().delete(triple);
            Graph raw = infGraph.getRawGraph();
            // deduction retractions should not remove asserted facts, so commented out next line
            // raw.delete(triple);
            if (raw.contains(triple)) {
                // Built in a graph which can't delete this triple
                // so block further processing of this delete to avoid loops
                
                deletesPending.remove(triple);
//                delete = false;
            }
        }
    }
    
    /**
     * Remove up the T-PBVs in the RETE network within the given time slot. 
     * @param start start of the time slot.
     * @param end end of the time slot.
     */
    public void sweepRETE(long start, long end){       
        // delete partial match
        Iterator keyIt = ruleConditions.keySet().iterator();
        
        // remove T-PBV within a time slot
        for(;keyIt.hasNext();){
            Rule rule = (Rule)keyIt.next();
            Iterator conditionIt = ruleConditions.getAll(rule);
            while(conditionIt.hasNext()){
                RETEClauseFilterNS condition = (RETEClauseFilterNS)conditionIt.next();
                for(int i=0; i<condition.continuations.size(); i++){
                    Object node = condition.continuations.get(i);
                    if(node instanceof RETEQueueNS)
                        ((RETEQueueNS)node).sweepBuffer(start, end);
                }
            }
        }
        
        // reset the tidied field to false after tidied up is finished
        keyIt = ruleConditions.keySet().iterator();
        for(;keyIt.hasNext();){
            Rule rule = (Rule)keyIt.next();
            Iterator conditionIt = ruleConditions.getAll(rule);
            while(conditionIt.hasNext()){
                RETEClauseFilterNS condition = (RETEClauseFilterNS)conditionIt.next();
                for(int i=0; i<condition.continuations.size(); i++){
                    Object node = condition.continuations.get(i);
                    if(node instanceof RETEQueueNS)
                        ((RETEQueueNS)node).finishSweep();
                }
            }
        }
    }
    
    /**
     * Increment the rule firing count, called by the terminal nodes in the
     * network.
     */
    protected void incRuleCount() {
        nRulesFired++;
    }
    
    /**
     * Find the next pending add triple.
     * @return the triple or null if there are none left.
     */
    protected synchronized Triple nextAddTriple() {
        int size = addsPending.size(); 
        if (size > 0) {         
            return (Triple)addsPending.remove(size - 1);
        }
        return null;
    }
    
    /**
     * Find the next pending add triple.
     * @return the triple or null if there are none left.
     */
    protected synchronized Triple nextDeleteTriple() {
        int size = deletesPending.size(); 
        if (size > 0) {
        	return (Triple)deletesPending.remove(size - 1);
        }
        return null;
    }

    // NOTE state is for printJoin use, should be remove in release
//    public int state = 0;
    /**
     * Process the queue of pending insert/deletes until the queues are empty.
     * Public to simplify unit tests - not normally called directly.
     */
    public void runAll() {
//        state ++;
        while(true) {
            boolean isAdd = false;
            Triple next = nextDeleteTriple();
            if (next == null) {
                next = nextAddTriple();
                isAdd = true;
            }
//            if(!isAdd){
//                System.err.println(" DEBUG (RETEEngine::runAll) : size of deletePending is "+deletesPending.size());
//            }
            if (next == null) {
                // Nothing more to inject, if this is a non-mon rule set now process one rule from the conflict set
                if (conflictSet.isEmpty()) {
                    return; // Finished
                } 
                
                // Wei: Never reach this in monotonic reasoning. RETEConflictSet.execute()
                // is called in RETEConflictSet.add().
                conflictSet.fireOne();
            } else {
//                if(state > 1){
//                    boolean temp = next instanceof TempTriple? true:false;
//                    System.err.println( (isAdd? "add ":"remove ")+(temp? "TempTriple: ":"Triple: ")+next);
//                }
                inject(next, isAdd);
            }
        }
    }
    
    /**
     * Wei: Singleton rules (rules with only one condition) are attached with terminal
     * node during alpha network compilation. Those rules will be fired in prematch
     * by inject(). Results will be populated into addsPendings and matched against
     * other conditions until no singleton rules can be fired.
     */
    public void preMatch(){
        while(true) {
            boolean isAdd = false;
            Triple next = nextDeleteTriple();
            if (next == null) {
                next = nextAddTriple();
                isAdd = true;
            }
            if (next == null) {
                // Nothing more to inject during prematch, then finish prematch
                return;
            } else {
                inject(next, isAdd);
            }
        }         
    }

    /**
     * Inject a single triple into the RETE network
     */
    private void inject(Triple t, boolean isAdd) {

        Iterator i1 = clauseIndex.getAll(t.getPredicate());

        Iterator i2 = clauseIndex.getAll(Node.ANY);

        Iterator i = new ConcatenatedIterator(i1, i2);
        while (i.hasNext()) {
//            System.err.println("fire " + t);
            ((FireTripleI)i.next()).fire(t, isAdd);      
        }
    }
    
//    /**
//     * This fires a triple into the current RETE network. 
//     * This format of call is used in the unit testing but needs to be public
//     * because the tester is in another package.
//     */
//    public void testTripleInsert(Triple t) {
//        Iterator i1 = clauseIndex.getAll(t.getPredicate());
//        Iterator i2 = clauseIndex.getAll(Node.ANY);
//        Iterator i = new ConcatenatedIterator(i1, i2);
//        while (i.hasNext()) {
//            RETEClauseFilter cf = (RETEClauseFilter) i.next();
//            cf.fire(t, true);
//        }
//    }
    
    /**
     * Scan the rules for any axioms and insert those
     */
    protected void findAndProcessAxioms() {
        for (Iterator i = rules.iterator(); i.hasNext(); ) {
            Rule r = (Rule)i.next();
            if (r.isAxiom()) {
                // An axiom
                RETERuleContext context = new RETERuleContext(infGraph, this);
                context.setEnv(new BindingVector(new Node[r.getNumVars()]));
                context.setRule(r);
                if (context.shouldFire(true)) {
                    RETEConflictSet.execute(context, true);
                }
                /*   // Old version, left during printJoin and final checks
                for (int j = 0; j < r.headLength(); j++) {
                    Object head = r.getHeadElement(j);
                    if (head instanceof TriplePattern) {
                        TriplePattern h = (TriplePattern) head;
                        Triple t = new Triple(h.getSubject(), h.getPredicate(), h.getObject());
                        addTriple(t, true);
                    }
                }
                */
            }
        }
        processedAxioms = true;
    }
    
//    /**
//     * Scan the rules for any run actions and run those
//     */
//    protected void findAndProcessActions() {
//        RETERuleContext tempContext = new RETERuleContext(infGraph, this);
//        for (Iterator i = rules.iterator(); i.hasNext(); ) {
//            Rule r = (Rule)i.next();
//            if (r.bodyLength() == 0) {
//                for (int j = 0; j < r.headLength(); j++) {
//                    Object head = r.getHeadElement(j);
//                    if (head instanceof Functor) {
//                        Functor f = (Functor)head;
//                        Builtin imp = f.getImplementor();
//                        if (imp != null) {
//                            tempContext.setRule(r);
//                            tempContext.setEnv(new BindingVector( r.getNumVars() ));
//                            imp.headAction(f.getArgs(), f.getArgLength(), tempContext);
//                        } else {
//                            throw new CororException("Invoking undefined Functor " + f.getName() +" in " + r.toShortString());
//                        }
//                    }
//                }
//            }
//        }
//    }

    public BasicForwardRuleInfGraph getInfGraph(){
        return infGraph;
    }
 
//=======================================================================
// Inner classes

//    /**
//     * Structure used in the clause index to indicate a particular
//     * clause in a rule. This is used purely as an internal data
//     * structure so we just use direct field access.
//     */
//    protected static class ClausePointer {
//        
//        /** The rule containing this clause */
//        protected Rule rule;
//        
//        /** The index of the clause in the rule body */
//        protected int index;
//        
//        /** constructor */
//        ClausePointer(Rule rule, int index) {
//            this.rule = rule;
//            this.index = index;
//        }
//        
//        /** Get the clause pointed to */
//        TriplePattern getClause() {
//            return (TriplePattern)rule.getBodyElement(index);
//        }
//    }
    
//    /**
//     * Structure used to wrap up processed rule indexes.
//     */
//    public static class RuleStore {
//    
//
//		/** MapImpl from predicate node to rule + clause, Node_ANY is used for wildcard predicates */
//        protected MultiArrayMap clauseIndex;
//    
//        /** List of predicates used in rules to assist in fast data loading */
//        public SetImpl predicatesUsed;
//        
//        /** Flag, if true then there is a wildcard predicate in the rule set so that selective insert is not useful */
//        protected boolean wildcardRule;
//        
//        /** True if all the rules are monotonic, so we short circuit the conflict set processing */
//        protected boolean isMonotonic = true;
//        
//        /** Constructor */
//        RuleStore(MultiArrayMap clauseIndex, SetImpl predicatesUsed, boolean wildcardRule, boolean isMonotonic) {
//            this.clauseIndex = clauseIndex;
//            this.predicatesUsed = predicatesUsed;
//            this.wildcardRule = wildcardRule;
//            this.isMonotonic = isMonotonic;
//        }
//    }  
}