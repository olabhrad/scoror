package ie.tcd.cs.nembes.coror.util;

import ie.tcd.cs.nembes.coror.util.ext.AbstractCollection;
import ie.tcd.cs.nembes.coror.shared.CororException;
import java.util.Arrays;


/**
 *  this class implements java.util.ArrayList, which is not present
 *  in the latest version of the J2ME library at the time we are
 *  implementing this code, and behaves the same.
 * 
 * issues:
 * 1. ArrayList is not yet inherent from AbstractCollection hence arguments that are supposed
 * to use collections are now using ArrayList.
 * 
 * @author Wei Tai
 */
public class ArrayList extends AbstractCollection{
    
    private final static int DEFAULT_SIZE = 10;
    
    private Object[] elementData;
    
    private int size;
    
    /** Creates a new instance of List */
    public ArrayList() {
        this(DEFAULT_SIZE);
    }
    
    public ArrayList(ArrayList l) {
        if( l == null) throw new CororException("cannot construct an arraylist from a null arraylist");
        elementData = new Object[(int)(l.size*1.1f)];
        System.arraycopy(l.elementData, 0, elementData, 0, size);
    }
    
    public ArrayList(int i) {
	elementData = new Object[i];
    }
    
    public boolean add(Object o) {
        ensureCapacity(size + 1);
        elementData[size++] = o;
        return true;
    }
    
    public void ensureCapacity(int minCapacity){
        int oldCapacity = elementData.length;
        if(minCapacity > oldCapacity){
            int newCapacity = oldCapacity + (oldCapacity >> 1);
            if(newCapacity < minCapacity) newCapacity = minCapacity;
            elementData = Arrays.copyOf(elementData, newCapacity);
        }
    }
    
    public boolean contains(Object o) {
        return indexOf(o) != -1;
    }
    
    /**
     * If two arraylists are equal.
     * @param o
     * @return true if equal and false otherwise
     */
    @Override
    public boolean equals(Object o) {
        if(o == this) return true;
        if(! (o instanceof ArrayList)) return false;
        ArrayList l = (ArrayList) o;
        if(! (l.size == size)) return false;
        
        return this.containsAll(l);
    }
    /**
     * Get the index for a specified object if exists.
     * @param o
     * @return the index if exists or -1 otherwise
     */
    public int indexOf(Object o) {  
        for(int i=0; i < size; i++){
            Object obj = elementData[i];
            if( obj == o || (o != null && o.equals(obj)))
                return i;
        }
        return -1;
    }
    
    public int lastIndexOf(Object o) {
        for(int i= size -1; i > -1; i--){
            Object obj = elementData[i];
            if( obj == o || (o != null && o.equals(obj)))
                return i;
        }
        return -1;        
    }

    public boolean isEmpty() {
        return size == 0;
    }
    
    public Iterator iterator() {
        return new Iterator(){

            private int index = 0;
            
            @Override
            public Object next() throws CororException {
                return elementData[index ++];
            }

            @Override
            public boolean hasNext() {
//                System.err.println("this is called");
                return index < size;
            }

            @Override
            public void remove() throws CororException {
                ArrayList.this.remove(--index);
            }

			@Override
			public void reset() {
				throw new UnsupportedOperationException("Not supported yet.");
				
			}
        };
    }
    
    public Object get(int index) {
        rangeCheck(index);
        return elementData[index];
    }
    
    public int size() {
        return size;
    }
    
    public boolean remove(Object o) {
        for(int i=0; i < size; i++){
            Object obj = elementData[i];
            if( obj == o || (o != null && o.equals(obj))){
                System.arraycopy(elementData, i+1, elementData, size, size - i - 1);
                elementData[--size] = null;
                return true;
            }
        }
        return false;
    }

    public Object remove(int index) {
        rangeCheck(index);
        Object oldElement = elementData[index];
        System.arraycopy(elementData, index+1, elementData, size, size - index - 1);
        elementData[--size] = null;
        return oldElement;
    }
    
    void rangeCheck(int index){
        if(index < 0 || index >= size) throw new CororException("out of bound exception");
    }
	
    /**
     * Copy the list into an array
     * @return
     */
    public Object[] toArray(){
        Object[] array = new Object[size];
        System.arraycopy(elementData, 0, array, 0, size);
        return array;
    }
	
    /**
     * Copy the list into the specified array 
     * @param a
     * @return
     */
    public Object[] toArray(Object[] a){
        if(a == null || a.length < size) a = new Object[size];
        System.arraycopy(elementData, 0, a, 0, size);
        if(a.length > size) a[size] = null;
        return a;
    }

    /**
     * append a collection at the end of this this in a correct sequence
     */
    public boolean addAll(ArrayList c) {
        ensureCapacity(size + c.size);
        for(int i = 0, j = size; i < c.size; i++, j++){
            elementData[j] = c.elementData[i];
        }
        size += c.size;
        return true;
    }
	
    /**
     * Add an element at specific position in the list.
     * @param index
     * @param element
     */
    public void add(int index, Object element){
        rangeCheckForAdd(index);
        ensureCapacity(size + 1);
        System.arraycopy(elementData, index, elementData, index+1, size-index);
        elementData[index] = element;
        size ++;
    }
    
    void rangeCheckForAdd(int index){ 
        if(index < 0 || index > size) throw new CororException("out of bound");        
    }
	
    public void clear() {
        for (int i=0; i<size; i++) elementData[i] = null;
        size = 0;
    }

    public boolean containsAll(ArrayList l) {
        Iterator e = l.iterator();
        while(e.hasNext())
            if(!contains(e.next())) return false;
        return true;
    }

    public boolean removeAll(AbstractCollection c) {
            throw new CororException("Unsupported operation");
    }

    public boolean retainAll(AbstractCollection c) {
            throw new CororException("Unsupported operation");
    }      

    @Override
    public int hashCode() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public String toString(){
        String str = "[";
        for(int i=0; i<size; i++){
            str += elementData[i].toString();
            if(i != size-1) str += ", ";
        }
        str += "]";
        return str;
    }
}
