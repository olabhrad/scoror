/*
 * ArrayMap.java
 *
 *  this class substitutes java.util.ArrayMap, which is not present
 *  in the latest versione of the J2ME library at the time we are
 *  implementing this code
 */

package ie.tcd.cs.nembes.coror.util;

import ie.tcd.cs.nembes.coror.util.ext.Map;
import ie.tcd.cs.nembes.coror.shared.CororException;
import ie.tcd.cs.nembes.coror.util.ext.AbstractCollection;
import ie.tcd.cs.nembes.coror.util.ext.AbstractSet;
import java.util.Arrays;

/**
 * A map implementation based on array
 * @author Wei tAI
 */
public class ArrayMap {
    
    private Entry[] elementData;
    
    private int size = 0;
    
    static final int DEFAULT_CAPACITY = 50;
    
    EntrySet entrySet;
    
    KeySet keySet;
    
    /** Creates a new instance of ArrayMap */
    public ArrayMap() {
	this(DEFAULT_CAPACITY);
    }
    
    public ArrayMap(ArrayMap other) {
	this();
	this.putAll(other);
    }
    
    public ArrayMap(int initialCapacity) {
	this.elementData = new Entry[initialCapacity];
    }
    
    public void clear() {
	for(int i=0; i<size; i++){
            elementData[i] = null;
        }
        size = 0;
    }
    
    public boolean containsKey(Object key) {
            return getEntry(key) != null;
    }
    
    Entry getEntry(Object key){
        if(key == null) throw new CororException("null key is not supported");
        for(int i = 0; i < size; i++){
            Entry e = elementData[i];
            if(e.key == key || key.equals(e.key)) return e;
        }
        return null;
    }
    
    public boolean containsValue(Object value) {
        if(value == null){
            for(int i = 0; i<elementData.length; i++)
                if(elementData[i].value == null) return true;
        } else {
            for(int i = 0; i<elementData.length; i++){
                Entry e = elementData[i];
                if(e.value == value || e.equals(value)) return true;
            }
        }
        return false;
    }
    
    public AbstractSet entrySet() {
	return entrySet != null? entrySet : (entrySet = new EntrySet());
    }
    
    public boolean equals(Object o) {
	if(o == this) return true;
        
        if(!(o instanceof ArrayMap))
	    return false;
        
        if(this.size() != ((ArrayMap)o).size())
            return false;
        
        ArrayMap m = (ArrayMap) o;
        
        Iterator i = entrySet().iterator();
        boolean eq = true;
        Entry aus;
        while(eq && i.hasNext()) {
            aus = (Entry)(i.next());
            if(!m.containsKey(aus.getKey()) || !m.containsValue(aus.getValue()))
                eq = false;
        }
        return eq;
    }
    
    public Object get(Object key) {
        if(key == null) throw new CororException("Null key is not supported");
	for(int i=0;i<size;i++) {
	    Entry e = elementData[i];
            if(key == e.key || key.equals(e.getKey()))
		return e.getValue();
	}
	return null;
    }
    
    public boolean isEmpty() {
	return size == 0;
    }
        
    public AbstractSet keySet() {
	return keySet != null? keySet : (keySet = new KeySet());
    }

    public Object put(Object newKey, Object newValue) {
        if(newKey == null) throw new CororException("Null key is not supported");
	boolean found = false;
	for(int i=0; i<size; i++) {
	    Entry e = elementData[i];
            Object prevValue = null;
	    if(newKey.equals(e.key)){
                prevValue = e.value;
                e.value = newValue;
                return prevValue;
            } 
	}
        if(size++ >= elementData.length) resize();
        elementData[size-1] = new Entry(newKey, newValue);
        return newValue;
    }
    
    void resize() {
        int oldCapacity = elementData.length;
        int newCapacity = oldCapacity + (oldCapacity >> 1);
        elementData = Arrays.copyOf(elementData, newCapacity);
    }
    
    public void putAll(ArrayMap m) {
	Iterator i = m.entrySet().iterator();
	Entry aus;
	while(i.hasNext()) {
	    aus = ((Entry)i.next());
	    this.put(aus.getKey(),aus.getValue());
	}
    }
    
    public boolean remove(Object key) {
	int index;
        if((index = getEntryIndex(key)) == -1) return false;
        removeAt(index);
        return true;
    }
    
    Entry removeAt(int index){
        if(index < 0 || index > size) throw new CororException("Out of bound");
        Entry e = elementData[index];
        System.arraycopy(elementData, index+1, elementData, index, size - index - 1);
        size --;
        return e;
    }
    
    int getEntryIndex(Object key){
        if(key == null) throw new CororException("null key is not supported");
        for(int i = 0; i < size; i++){
            Entry e = elementData[i];
            if(e.key == key || key.equals(e.key)) return i;
        }
        return -1;        
    }
    
    public int size() {
	return size;
    }
    
    Values values;
    
    public AbstractCollection values() {
	return values != null? values : (values = new Values());
    }
    
    public static class Entry {

	protected Object key, value;

	/** Creates a new instance of Entry */
	public Entry(Object newKey, Object newValue) {
	    if(newKey == null || newValue == null)
		throw new CororException("Map::Entry::Entry -- neither key or value can be NULL");
	    else {
		key = newKey;
		value = newValue;	    
	    }
	}

	public Object getKey() {
	    return key;
	}

	public Object getValue() {
	    return value;
	}

	public void setKey(Object newKey) {
	    key = newKey;
	}

	public void setValue(Object newValue) {
	    value = newValue;
	}

	public boolean equals(Object other) {
	    if(!(other instanceof Map.Entry))
		return false;
	    else
		if(((Map.Entry)other).getKey().equals(this.key) && ((Map.Entry)other).getValue().equals(this.value))
		    return true;
		else
		    return false;
	}
    }
        
    private abstract class ArrayIterator implements Iterator {

        int index = 0;

        @Override
        public boolean hasNext() {
            return index < size;
        }

        @Override
        public void remove() throws CororException {
            ArrayMap.this.removeAt(index);
        }
        
        Entry nextEntry(){
            return elementData[index++];
        }
        
    }    
        
    private class EntrySet extends AbstractSet {

        @Override
        public Iterator iterator() {
            return new ArrayIterator(){

                @Override
                public Object next() throws CororException {
                    return nextEntry();
                }

				@Override
				public void reset() {
					throw new UnsupportedOperationException("Not supported yet.");
					
				}
            };
        }

        @Override
        public int size() {
            return size;
        }

        @Override
        public boolean add(Object o) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }
    }
    
    private class KeySet extends AbstractSet {

        @Override
        public Iterator iterator() {
            return new ArrayIterator(){

                @Override
                public Object next() throws CororException {
                    return nextEntry().key;
                }

				@Override
				public void reset() {
					// TODO Auto-generated method stub
					
				}
                
            };
        }

        @Override
        public int size() {
            return size;
        } 

        @Override
        public boolean add(Object o) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }
    }
    
    private class Values extends AbstractCollection{

        @Override
        public boolean equals(Object o) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public int hashCode() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public Iterator iterator() {
            return new ArrayIterator(){

                @Override
                public Object next() throws CororException {
                    return nextEntry().value;
                }
                @Override
                public void reset(){
                	throw new UnsupportedOperationException("Not supported yet.");
                }
                
            };
        }

        @Override
        public int size() {
            return size;
        }

        @Override
        public boolean add(Object o) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }
        
    }
}
