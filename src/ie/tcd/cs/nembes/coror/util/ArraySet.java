package ie.tcd.cs.nembes.coror.util;

import ie.tcd.cs.nembes.coror.shared.CororException;
import ie.tcd.cs.nembes.coror.util.ext.AbstractCollection;
import ie.tcd.cs.nembes.coror.util.ext.AbstractSet;

/**
 * An implementation of set.
 * @author Wei Tai
 */
public class ArraySet extends AbstractSet{
    
    private ArrayMap elementData;
    
    private final static Object VALUE = new Object();
    
    public ArraySet() {
	elementData = new ArrayMap(30);
    }
    
    public ArraySet(ArraySet set) {
	if(set == null) throw new CororException("Attempt to create set from a null set");
        int size = set.size();
        elementData = new ArrayMap(size);
        for(Iterator i = set.iterator(); i.hasNext();){
            add0(i.next());
        }
    }
    
    private boolean add0(Object o){
	elementData.put(o, VALUE);
        return true;        
    }
    
    public boolean add(Object o) {
        return add0(o);
    }
    
    public int size(){
        return elementData.size();
    }

    public boolean addAll(AbstractCollection c) {
        for(Iterator i = c.iterator(); i.hasNext();){
            elementData.put(i.next(), VALUE);
        }
        return true;
    }

    public boolean containsAll(AbstractCollection c) {
        for(Iterator i = c.iterator(); i.hasNext(); )
            if(! elementData.containsKey(i.next())) return false;
        return true;
    }

    public boolean removeAll(AbstractCollection c) {
        boolean removed = false;
        for(Iterator i = c.iterator(); i.hasNext(); )
            return elementData.remove(i.next());
        return removed;
    }

    public boolean retainAll(AbstractCollection c) {
        boolean modified = false;
        for(Iterator i = elementData.keySet().iterator(); i.hasNext();){
            ArrayMap.Entry e = (ArrayMap.Entry)i.next();
            if(!c.contains(e.getKey())) {
                elementData.remove(e);
                modified = true;
            }
        }
        return modified;
    }

    public Iterator iterator() {
        return elementData.keySet().iterator();
    }
}
