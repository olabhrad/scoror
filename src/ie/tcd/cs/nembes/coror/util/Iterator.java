package ie.tcd.cs.nembes.coror.util;

import ie.tcd.cs.nembes.coror.shared.CororException;


/** this interface substitutes java.util.Iterator, which is not present
 *  in the latest versione of the J2ME library at the time we are
 *  implementing this code
 */
public interface Iterator {
    public Object next() throws CororException;
    public boolean hasNext();
    public void remove() throws CororException;
    public void reset();
}