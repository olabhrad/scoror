package ie.tcd.cs.nembes.coror.util;

import ie.tcd.cs.nembes.coror.shared.CororException;
import ie.tcd.cs.nembes.coror.util.ext.AbstractCollection;
import ie.tcd.cs.nembes.coror.util.ext.AbstractSet;
import ie.tcd.cs.nembes.coror.util.ext.Map;
import ie.tcd.cs.nembes.coror.util.iterator.NullIterator;
import java.util.Arrays;


/**
 * An extension to a standard map that supports one-to-many mappings: that is, there
 * may be zero, one or many vList corresponding to a given key.
 *
 * @author Wei Tai (<a href="mailto:wtai@scss.tcd.ie">email</a>)
 */
public class MultiArrayMap implements Map
{

//    /** Encapsulated hash table stores the vList */
//    private ArrayMap table;
//
//    private Values values;
//    
//    // Constructors
//    //////////////////////////////////
//
//    /**
//     * <p>Construct a new empty one-to-many map</p>
//     */
//    public MultiArrayMap() {
//        table = new ArrayMap();
//    }
//    
//    
//    /**
//     * <p>Construct a new one-to-many map whose contents are
//     * initialised from the existing map.</p>
//     *
//     * @param map An existing one-to-many map
//     */
//    public MultiArrayMap( MultiArrayMap map ) {
//        
//        for (Iterator i = map.keySet().iterator();  i.hasNext(); ) {
//            Object key = i.next();
//
//            for (Iterator j = map.getAll( key );  j.hasNext();  ) {
//                put( key, j.next() );
//            }
//        }
//    }
//
//
//
//
//    // External signature methods
//    //////////////////////////////////
//
//    /**
//     * Clear all entries from the map.
//     */
//    public void clear() {
//        table.clear();
//    }
//
//
//    /**
//     * Answer true if the map contains the given value as a key.
//     *
//     * @param key The key object to test for
//     * @return True or false
//     */
//    public boolean containsKey( Object key ) {
//        return table.containsKey( key );
//    }
//
//
//    /**
//     * Answer true if the map contains the given object as a value
//     * stored against any key. Note that this is quite an expensive
//     * operation in the current implementation.
//     *
//     * @param value The value to test for
//     * @return True if the value is in the map
//     */
//    public boolean containsValue( Object value ) {
//        for (Iterator vList = table.values().iterator();  vList.hasNext(); ) {
//            Object x = vList.next();
//            if (x == vList || (value != null && vList.equals( x ))) return true;
//        }
//        return false;
//    }
//
//
//    /**
//     * <p>Answer true if this mapping contains the pair
//     * <code>(key,&nbsp;value)</code>.</p>
//     * @param key A key object
//     * @param value A value object
//     * @return True if <code>key</code> has <code>value</code>
//     * as one of its vList in this mapping
//     */
//    public boolean contains( Object key, Object value ) {
//        for (Iterator i = getAll( key ); i.hasNext(); ) {
//            if (i.next().equals( value )) {
//                return true;
//            }
//        }
//        return false;
//    }
//    
//    
//    /**
//     * Answer a set of the mappings in this map.  Each member of the set will
//     * be a Key - AbstractCollection pair.
//     */
//    public AbstractSet entrySet() {
//        return table.entrySet();
//    }
//
//
//    /**
//     * Compares the specified object with this map for equality.
//     * Returns true if the given object is also a map and the two Maps
//     * represent the same mappings. More formally, two maps t1 and t2 represent
//     * the same mappings if t1.entrySet().equals(t2.entrySet()).
//     *
//     * This ensures that the equals method works properly across different
//     * implementations of the ArrayMap interface.
//     *
//     * @param o The object to be compared for equality with this map.
//     * @return True if the specified object is equal to this map.
//     */
//    public boolean equals( Object o ) {
//        if (o instanceof ArrayMap) {
//            return entrySet().equals( ((ArrayMap) o).entrySet() );
//        }
//        else
//            return false;
//    }
//
//
//    /**
//     * Get a value for this key.  Since this map is explicitly designed to
//     * allow there to be more than one mapping per key, this method will return
//     * an undetermined instance of the mapping. If no mapping exists, or the
//     * selected value is null, null is returned.
//     *
//     * @param key The key to access the map.
//     * @return One of the vList this key corresponds to, or null.
//     * @see #getAll
//     */
//    public Object get( Object key ) {
//        ArrayList entry = (ArrayList) table.get( key );
//
//        if (entry != null) {
//            if (!entry.isEmpty()) {
//                return entry.get( 0 );
//            }
//        }
//
//        // not present
//        return null;
//    }
//
//
//    /**
//     * Answer an iterator over all of the vList for the given key.  An iterator
//     * is always supplied, even if the key is not present.
//     *
//     * @param key The key object
//     * @return An iterator over all of the vList for this key in the map
//     */
//    public Iterator getAll( Object key ) {
//        ArrayList entry = (ArrayList) table.get( key );
//        return (entry != null) ? entry.iterator() : NullIterator.instance;
//    }
//
////    /**
////     * Answer a list containing vList put under this key.
////     * @param key The key object
////     * @return the value list corresponding to the given key
////     */
////    public ArrayList getAllAsList(Object key) {
////        return (ArrayList) table.get(key);
////    }
//
//    /**
//     * Returns the hash code value for this map. The hash code of a map is
//     * defined to be the sum of the hashCodes of each entry in the map's
//     * entrySet view. This ensures that t1.equals(t2) implies
//     * that t1.hashCode()==t2.hashCode() for any two maps t1 and t2,
//     * as required by the general contract of Object.hashCode
//     */
//    public int hashCode() {
//        int hc = 0;
//
//        for (Iterator i = entrySet().iterator();  i.hasNext(); ) {
//            hc ^= i.next().hashCode();
//        }
//
//        return hc;
//    }
//
//
//    /**
//     * Answer true if the map is empty of key-value mappings.
//     *
//     * @return True if there are no entries.
//     */
//    public boolean isEmpty() {
//        return table.isEmpty();
//    }
//
//
//    /**
//     * Answer a set of the keys in this map
//     *
//     * @return The keys of the map as a SetImpl
//     */
//    public AbstractSet keySet() {
//        return table.keySet();
//    }
//
//
//    /**
//     * Associates the given value with the given key.  Since this map formulation
//     * allows many vList for one key, previous associations with the key are not
//     * lost.  Consequently, the method always returns null (since the replaced value
//     * is not defined).
//     *
//     * @param key The key object
//     * @param value The value object
//     * @return Null.
//     */
//    public Object put( Object key, Object value ) {
//        ArrayList entries = (ArrayList) table.get( key );
//        entries = entries == null ? new ArrayList() : entries;
//
//        // add the new value to the list of vList held against this key
//        entries.add( value );
//        table.put( key, entries );
//
//        return null;
//    }
//
//
//    /**
//     * Replace current entry list for the given key with a new entry list.
//     * @param key The key object
//     * @param entries The new value list
//     */
//    public void replace(Object key, ArrayList entries) {
//        table.remove(key);
//        table.put(key, entries);
//    }
//    
//    /**
//     * <p>Put all entries from one map into this map. This method is for normal ArrayMap</p>
//     * @param m The map whose contents are to be copied into this map
//     */
//    public void putAll( ArrayMap m ) {
////        boolean many = (m instanceof MultiArrayMap);
////        
////        for (Iterator i = m.keySet().iterator(); i.hasNext(); ) {
////            Object key = i.next();
////            if (many) {
////                for (Iterator j = ((MultiArrayMap) m).getAll( key ); j.hasNext(); ) {
////                    put( key, j.next() );
////                }
////            }
////            else {
////                put( key, m.get( key ) );
////            }
////        }
//        for (Iterator i = m.keySet().iterator(); i.hasNext(); ) {
//            Object key = i.next();
//            put( key, m.get( key ) );
//        }
//    }
//
//    /**
//     * <p>Put all entries from one map into this map. This method is for MultiArrayMap</p>
//     * @param m The map whose contents are to be copied into this map
//     */
//    public void putAll( MultiArrayMap m) {
//        for (Iterator i = m.keySet().iterator(); i.hasNext(); ) {
//            Object key = i.next();
//            for (Iterator j = ((MultiArrayMap) m).getAll( key ); j.hasNext(); ) {
//                put( key, j.next() );
//            }
//        }
//    }
//
//    /**
//     * Remove all of the associations for the given key.  If only a specific
//     * association is to be removed, use {@link #remove( java.lang.Object, java.lang.Object )}
//     * instead.  Has no effect if the key is not present in the map.  Since no
//     * single specific association with the key is defined, this method always
//     * returns null.
//     *
//     * @param key All associations with this key will be removed
//     * @return null
//     */
//    public boolean remove( Object key ) {
//        return table.remove( key );
//    }
//
//
//    /**
//     * <p>Remove the specific association between the given key and value. Has
//     * no effect if the association is not present in the map. If all vList
//     * for a particular key have been removed post removing this particular
//     * association, the key will no longer appear as a key in the map.</p>
//     *
//     * @param key The key object
//     * @param value The value object
//     */
//    public boolean remove( Object key, Object value ) {
//        ArrayList entries = (ArrayList) table.get( key );
//        boolean modified = false;
//        if (entries != null) {
//            modified = entries.remove( value );
//            
//            if (entries.isEmpty()) {
//                table.remove( key );
//            }
//        }
//        return modified;
//    }
//
//
//    /**
//     * <p>Answer the number of key-value mappings in the map</p>
//     * @return The number of key-value pairs.
//     */
//    public int size() {
//        int size = 0;
//
//        for (Iterator i = table.keySet().iterator();  i.hasNext();  ) {
//            size += ((ArrayList) table.get( i.next() )).size();
//        }
//
//        return size;
//    }
//
//
//    /**
//     * <p>Returns a collection view of the vList contained in this map.
//     * Specifically, this will be a set, so duplicate vList that appear
//     * for multiple keys are suppressed.</p>
//     * @return A set of the vList contained in this map.
//     */
//    public AbstractCollection values() {
//        return values != null? values: (values = new Values());
//    }
//
//    /**
//     * <p>Answer a string representation of this map. This can be quite a long string for
//     * large maps.<p>
//     */
//    public String toString() {
//        StringBuffer buf = new StringBuffer( "OneToManyMap{" );
//        String sep = "";
//        
//        for (Iterator i = keySet().iterator(); i.hasNext(); ) {
//            Object key = i.next();
//            buf.append( sep );
//            buf.append( key );
//            buf.append( "={" );
//            
//            String sep1 = "";
//            for (Iterator j = getAll(key); j.hasNext(); ) {
//                buf.append( sep1 );
//                buf.append( j.next() );
//                sep1=",";
//            }
//            buf.append("}");
//            sep=",";
//        }
//        buf.append("}");
//        return buf.toString();
//    }
//
//    @Override
//    public void putAll(Map m) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//    }
//    
//    private class Values extends AbstractCollection {
//
//        Iterator valueListIterator = null;
//        
//        Values(){
//            valueListIterator = table.values().iterator();
//        }
//        
//        @Override
//        public boolean add(Object o) {
//            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//        }
//
//        @Override
//        public boolean equals(Object o) {
//            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//        }
//
//        @Override
//        public int hashCode() {
//            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//        }
//
//        @Override
//        public Iterator iterator() {
//            return new ValuesIterator();
//        }
//
//        private class ValuesIterator implements Iterator{
//
//            Iterator currentIterator;
//
//            ValuesIterator(){
//                currentIterator = nextIterator();
//                if(currentIterator == null) currentIterator = NullIterator.instance;
//            }
//
//            @Override
//            public Object next() throws CororException {
//                return currentIterator.next();
//            }
//
//            @Override
//            public boolean hasNext() {
//                return currentIterator.hasNext() || 
//                        ( (currentIterator = nextIterator()) != null && 
//                        currentIterator.hasNext());
//            }
//
//            @Override
//            public void remove() throws CororException {
//                currentIterator.remove();
//            }   
//            
//            private Iterator nextIterator(){
//                if(valueListIterator.hasNext()) {
//                    AbstractCollection nextValueList = (AbstractCollection)valueListIterator.next();
//                    return nextValueList.iterator();
//                }
//                return null;
//            }
//        }
//        
//        @Override
//        public int size() {
//            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//        }
//        
//    }
    
    private Entry[] elementData;
    
    private int size = 0;
    
    private int keySize = 0;
    
    static final int DEFAULT_KEY_CAPACITY = 30;
    
    static final int DEFAULT_VALUE_CAPACITY = 5;
    
    /** Creates a new instance of ArrayMap */
    public MultiArrayMap() {
	this(DEFAULT_KEY_CAPACITY);
    }
    
    public MultiArrayMap(Map other) {
	this();
	this.putAll(other);
    }
    
    public MultiArrayMap(int initialCapacity) {
	this.elementData = new Entry[initialCapacity];
    }
    
    public void clear() {
	for(int i=0; i<keySize; i++){
            elementData[i] = null;
        }
        keySize = 0;
        size = 0;
    }
    
    public boolean containsKey(Object key) {     
        return getEntry(key) != null;
    }
    
    /**
     * Get the last inserted entry with the specified key. 
     * @param key the key for which entry is to get
     * @return 
     */
    Entry getEntry(Object key){

        for(int i = 0; i < keySize; i++){
            Entry e = elementData[i];
            if(e.key == key || (key != null && key.equals(e.key))) return e;
        }
        return null;
    }
    
    @Override
    public boolean containsValue(Object value) {
        for(int i = 0; i<keySize; i++){
            for(Entry e = elementData[i]; e != null; e = e.next){
                if (e.value == value || (value != null && value.equals(e.value))) return true;
            }
        }
        return false;
    }
    
    public boolean equals(Object o) {
        
	if(o == this) return true;
        
        if(!(o instanceof MultiArrayMap))
	    return false;
        
        MultiArrayMap m = (MultiArrayMap) o;    
        
        if(size != m.size || keySize != m.keySize)
            return false;
        
        Iterator i = entrySet().iterator();
        
        boolean eq = true;
        Entry aus;
        while(eq && i.hasNext()) {
            aus = (Entry)(i.next());
            if(!m.containsKey(aus.getKey()) || !m.containsValue(aus.getValue()))
                eq = false;
        }
        return eq;
    }
    
    /**
     * Get the value of the last inserted entry with the specified key.
     * @param key
     * @return the value for the last inserted entry
     */
    public Object get(Object key) {
        
	for(int i=0;i<keySize;i++) {
            Entry e = elementData[i];
            if(key == e.key || (key != null && key.equals(e.key))){
                return e.value;
            }
	}
	return null;
    }
    
    /**
     * Get all the values for the specified key.
     * @param key
     * @return An iterator allowing iterating all the entries with the specified
     * key.
     */
    public Iterator getAll(Object key){
        
	for(int i=0;i<keySize;i++) {
	    Entry e = elementData[i];
            if(key == e.key || (key != null && key.equals(e.key))){
                return new KeyValuesIterator(i);
            }
	}
        return NullIterator.instance;
    }
    
    public boolean isEmpty() {
	return size == 0;
    }

    public Object put(Object newKey, Object newValue) {

	for(int i=0; i<keySize; i++) {
	    Entry e = elementData[i];
            Object key = e.key;
            // newKey already exists
	    if(newKey == e.key || (newKey != null && newKey.equals(e.key))){
                // find the last entry
                for(; e.next != null; e = e.next){
                    ;
                }
                // newKey exists but not newValue, insert the entry at the begining
                Entry newEntry = new Entry(newKey, newValue);
                e.next = newEntry;
                size ++;
                return null;
            } 
	}
        
        if(keySize++ >= elementData.length) resize();
        elementData[keySize-1] = new Entry(newKey, newValue);
        size ++;
        return null;
    }
    
    void resize() {
        int oldCapacity = elementData.length;
        int newCapacity = oldCapacity + (oldCapacity >> 1);
        elementData = Arrays.copyOf(elementData, newCapacity);
    }
    /**
     * This method does not handle the situation when m is a MultiArrayMap
     * @param m 
     */
    @Override
    public void putAll(Map m) {
        throw new CororException("not perperly implemented");
//        if(m instanceof MultiArrayMap) throw new CororException("Does not handle MultiArrayMap in this method for the moment");
//	Iterator i = m.entrySet().iterator();
//	ArrayMap.Entry aus;
//	while(i.hasNext()) {
//	    aus = ((ArrayMap.Entry)i.next());
//	    this.put(aus.getKey(),aus.getValue());
//	}
    }
    /**
     * Remove all the entries with the specified key.
     * @param key
     */
    @Override
    public Object remove(Object key) {
	int index;
        if((index = getEntryIndex(key)) == -1) return false;
        removeAt(index);
        return true;
    }
    
    /**
     * Remove a specific key/value pair
     * @param key
     * @param value
     * @return true if key/value pair is removed and false otherwise
     */
    public boolean remove(Object key, Object value){
        for(int i=0; i<keySize; i++){
            Entry e = elementData[i];
            if(e.key == key || ((key != null) && key.equals(e.key))){
                Entry prev = null;
                for(;e != null; e = e.next){
                    // a match with key/value is found
                    if(e.value == value || (value != null && value.equals(e.value))){
                        if(prev == null){
                            // the match is the first entry in the list
                            elementData[i] = e.next;
                            size --;
                            // if no more entries left for this key, remove the key
                            if(elementData[i] == null) {
                                System.arraycopy(elementData, i+1, elementData, i, keySize - i - 1);
                                keySize --;
                            }
                            return true;
                        } else {
                            // the match is not the first entry
                            prev.next = e.next;
                            return true;
                        } 
                    }
                    prev = e;
                }
            }
        }
        return false;
    }
    
    /**
     * Remove the entry at the position specified by index.
     */
    Entry removeAt(int index){
        if(index < 0 || index > keySize) throw new CororException("Out of bound");
        Entry e = elementData[index];
        System.arraycopy(elementData, index+1, elementData, index, keySize - index - 1);
        keySize --;
        int i = 0;
        for(Entry entry = e; entry != null; entry = entry.next) i++;
        size -= i;
        return e;
    }
    
    
    
    int getEntryIndex(Object key){
        for(int i = 0; i < keySize; i++){
            Entry e = elementData[i];
            if(e.key == key || (key != null && key.equals(e.key))) return i;
        }
        return -1;        
    }
    
    public int size() {
	return size;
    }
    
    public int keySize() {
        return keySize;
    }
    
    public static class Entry implements Map.Entry{

        Object key;
        Object value;
        Entry next;
        
	public Entry(Object key, Object value) {
            this.key = key;
            this.value = value;
	}

	public Object getKey() {
	    return key;
	}

	public Object getValue() {
	    return value;
	}

	public void setKey(Object newKey) {
	    key = newKey;
	}

	public void setValue(Object newValue) {
	    value = newValue;
	}

	public boolean equals(Object other) {
            if(other == null) return false;
            
            if(!(other instanceof Map.Entry))
		return false;
	    
            Map.Entry e = (Map.Entry) other;
            Object k = e.getKey();
            Object v = e.getValue();
            
            return (((k == key) || (k != null && k.equals(key))) && 
                    ((v == value) || (v != null && v.equals(value))));
	}
        
        public String toString(){
            return "{"+key.toString()+","+value.toString()+"}";
        }
    }
        
    private abstract class EntryIterator implements Iterator {

        int keyIndex = 0;
        Entry nextEntry = elementData[0];
        Entry curEntry = null;
        
        @Override
        public boolean hasNext() {
            return nextEntry != null;
        }

        @Override
        public void remove() throws CororException {
            MultiArrayMap.this.remove(curEntry.key, curEntry.value);
        }
        
        Entry nextEntry(){
            curEntry = nextEntry;
            if(nextEntry == null) throw new CororException("Iterator out of bound");
            try{
                nextEntry = curEntry.next == null?elementData[++keyIndex]:curEntry.next;
            } catch(ArrayIndexOutOfBoundsException e){
                nextEntry = null;
            }
            
            return curEntry;
        }
        
    }    

        
    EntrySet entrySet;

        
    /**
     * Get a set of key/Collection pairs. In the MultiArrayList this is of
     * little use.
     * @return a set of key-AbstractCollection pair
     */
    public AbstractSet entrySet() {
	return entrySet != null? entrySet : (entrySet = new EntrySet());
    }
    
    private class EntrySet extends AbstractSet {

        @Override
        public Iterator iterator() {
            return new EntryIterator(){

                public Object next() throws CororException {
                    return nextEntry();
                }

				@Override
				public void reset() {
					throw new UnsupportedOperationException("Not supported yet.");
					
				}
            };
        }

        @Override
        public int size() {
            return size;
        }

        @Override
        public boolean add(Object o) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }
    }
    
    KeySet keySet;
    
    public AbstractSet keySet() {
	return keySet != null? keySet : (keySet = new KeySet());
    }
    
    private class KeySet extends AbstractSet {

        int keyIndex;
        
        @Override
        public Iterator iterator() {
            return new Iterator(){

                @Override
                public Object next() throws CororException {
                    return elementData[keyIndex++].key;
                }

                @Override
                public boolean hasNext() {
                    return keyIndex < keySize;
                }

                @Override
                public void remove() throws CororException {
                    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                }

				@Override
				public void reset() {
					throw new UnsupportedOperationException("Not supported yet.");
					
				}
                
            };
        }

        @Override
        public int size() {
            return keySize;
        } 

        @Override
        public boolean add(Object o) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }
        
        @Override
        public boolean remove(Object o) {
            throw new CororException("calling this method will remove the entire enty");
        }
    }
    
    Values values;
    
    public AbstractCollection values() {
	return values != null? values : (values = new Values());
    }  
    
    private class Values extends AbstractCollection {
        
        @Override
        public boolean add(Object o) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public boolean equals(Object o) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public int hashCode() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public Iterator iterator() {
            return new EntryIterator(){

                @Override
                public Object next() throws CororException {
                    return nextEntry().value;
                }

				@Override
				public void reset() {
					throw new UnsupportedOperationException("Not supported yet.");
					
				}
                
            };
        }
        
        @Override
        public int size() {
            return size;
        }
        
    }   
    
    private class KeyValuesIterator implements Iterator {
    	Entry startEntry;
        Entry curEntry;
        
        KeyValuesIterator(int k){ 	
            startEntry = elementData[k];
            curEntry = startEntry;
        }
        
        @Override
        public Object next() throws CororException {
            if(curEntry == null) throw new CororException("iterator out of bound");
            Object e = curEntry.value;
            curEntry = curEntry.next;
            return e;
        }

        @Override
        public boolean hasNext() {
            return curEntry != null;
        }

        @Override
        public void remove() throws CororException {
            MultiArrayMap.this.remove(curEntry.key, curEntry.value);
        }
        
        @Override
        public void reset(){
        	curEntry = startEntry;
        }
        
    }
    
    public String toString(){
        String output = "Start of MultiArrayMap \n";
        for (int i=0; i<keySize; i++){
            Entry e = elementData[i];
            output += e.key + " : ";
            for(;e != null; e = e.next){
                output += e.toString() + " - ";
//                System.err.println("Entry "+i+" is "+e.toString());
            }
            output += "\n";
        }
        output += ("End of MultiArrayMap \n");
        return output;
    }
    
    /**
     * Test
     */
    public static void main(String[] args){
        String [][] data = {{"1", "1a"}, {"1", "1b"},  {"1", "1c"}, {"1", "1d"},
                            {"2", "2a"}, {"2", "2b"},  {"2", "2c"}, {"2", "2d"},
                            {"3", "3a"}, {"3", "3b"},  {"3", "3c"}, {"3", "3d"},
                            {"4", "4a"}, {"4", "4b"},  {"4", "4c"}, {"4", "4d"}, {"4", "4d"}};
        
        MultiArrayMap map = new MultiArrayMap();
        
        for(int i=0; i<data.length; i++){
            map.put(data[i][0], data[i][1]);
//            System.err.println("key = "+data[i][0] + " value = " + data[i][1]);
        }
        
        System.err.println(map.toString());
        
        Iterator it = map.getAll("4");
        for(;it.hasNext();){
            System.err.println(it.next());
        }
    }
    
}

