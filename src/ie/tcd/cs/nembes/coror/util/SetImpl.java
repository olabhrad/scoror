/*
 * SetImpl.java
 *
 *  this class substitutes java.util.SetImpl, which is not present
 *  in the latest versione of the J2ME library at the time we are
 *  implementing this code
 */

package ie.tcd.cs.nembes.coror.util;

import ie.tcd.cs.nembes.coror.util.ext.AbstractCollection;
import ie.tcd.cs.nembes.coror.util.iterator.IteratorImpl;
import java.util.Vector;

/**
 *
 * @author ilBuccia
 */
public class SetImpl extends List{
    
    /** Creates a new instance of SetImpl */
    public SetImpl() {
	super();
    }
    
    public SetImpl(Vector newVector) {
	Iterator it = new IteratorImpl(newVector);
	while(it.hasNext())
	    this.add(it.next());
    }
    
    public boolean add(Object o) {
	if(this.contains(o))
	    return false;
	else
	    v.addElement(o);
	return true;
    }
    
    /**
     * J2ME Version
     */
	public SetImpl(SetImpl s){
		for(Iterator i = s.iterator(); i.hasNext();){
			v.addElement(i.next());
		}
	}
	
	public boolean addAll(AbstractCollection c) {
		for(Iterator i = c.iterator(); i.hasNext();){
			v.addElement(i.next());
		}
		return true;
	}

	public void clear() {
                v.removeAllElements();
	}

	public boolean containsAll(AbstractCollection c) {
		return false;
	}

	public boolean removeAll(AbstractCollection c) {
		return false;
	}

	public boolean retainAll(AbstractCollection c) {
		return false;
	}

	public Object[] toArray() {
		return null;
	}

	public Object[] toArray(Object[] a) {
		return null;
	}
}
