package ie.tcd.cs.nembes.coror.util.ext;

import ie.tcd.cs.nembes.coror.shared.CororException;
import ie.tcd.cs.nembes.coror.util.Iterator;
import java.util.Arrays;

/**
 * A J2ME version of the abstract class java.util.AbstractCollection.
 * @author Wei Tai
 */
public abstract class AbstractCollection {
	
	public abstract boolean add(Object o);
	
	public boolean addAll(AbstractCollection c){
            boolean modified = false;
            Iterator e = c.iterator();
            while(e.hasNext()) {
                if(add(e.next())) modified = true;
            }
            return modified;
        }
	
	public void clear(){
            Iterator e = iterator();
            while(e.hasNext()) {
                e.next();
                e.remove();
            }
        }
	
	public boolean contains(Object o){
            Iterator e = iterator();
            if(o == null){
                while(e.hasNext())
                    if(e.next() == null)
                        return true;
            } else {
                while(e.hasNext())
                    if(o.equals(e.next()))
                        return true;
            }
            return false;
        }
	
	public boolean containsAll(AbstractCollection c){
            Iterator e = c.iterator();
            while(e.hasNext())
                if(!contains(e.next())) return false;
            return true;
        }
	
	public abstract boolean equals(Object o);
	
	public abstract int hashCode();
	
	public boolean isEmpty(){
            return size() == 0;
        }
	
	public abstract Iterator iterator();
	
	public boolean remove(Object o){
            Iterator e = iterator();
            if(o == null){
                while(e.hasNext()){
                    if(e.next() == null){
                        e.remove();
                        return true;
                    }
                }
            } else{
                while(e.hasNext()) {
                    if(o.equals(e.next())) {
                        e.remove();
                        return true;
                    }
                }
            }
            return false;
        }
	
	public boolean removeAll(AbstractCollection c){
            boolean modified = false;
            Iterator e = iterator();
            while(e.hasNext()){
                if(c.contains(e.next())){
                    e.remove();
                    modified = true;
                }
            }
            return modified;
        }
	
	public boolean retainAll(AbstractCollection c){
            boolean modified = false;
            Iterator e = iterator();
            while(e.hasNext()){
                if(!c.contains(e.next())){
                    e.remove();
                    modified = true;
                }
            }
            return modified;
        }
	
	public abstract int size();
	
	public Object[] toArray(){
            Object[] r = new Object[size()];
            Iterator it = iterator();
            for(int i=0; i<r.length; i++){
                if(! it.hasNext()) return r;
                r[i] = it.next();
            }
            return r;
        }
	
	public Object[] toArray(Object[] a){
            throw new CororException("Unsupported operation");
        }
}
