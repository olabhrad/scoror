/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ie.tcd.cs.nembes.coror.util.ext;

import ie.tcd.cs.nembes.coror.shared.CororException;
import ie.tcd.cs.nembes.coror.util.Iterator;
import ie.tcd.cs.nembes.coror.util.ArraySet;
import java.util.Vector;

/**
 * @author WEI TAI
 */
public class HashMap implements Map{
    
    static final int MAXIMUM_CAPACITY = 1 << 30;
    
    static final int DEFAULT_INITIAL_CAPACITY = 16;
    
    static final float DEFAULT_LOAD_FACTOR = 0.75f;
    
    Entry[] table;
    
    int size;
    
    float loadFactor;
    
    AbstractSet keySet = null;
    
    AbstractCollection values = null;
    
    private AbstractSet entrySet = null;
    
    
    
    public HashMap(){
        throw new UnsupportedOperationException("Not supported yet.");  
    }
    
    public HashMap(int initialCapacity){
        this(initialCapacity, DEFAULT_LOAD_FACTOR);
    }
    
    public HashMap(int initialCapacity, float loadFactor){
        // find a power of 2 >= initialCapacity
        int capacity = 1;
        while(capacity < initialCapacity) capacity <<= 1;
        table = new Entry[capacity];
    }
    
    public HashMap(Map m){
        throw new UnsupportedOperationException("Not supported yet.");         
    }
    
    /**
     * Apply a supplement hash function to a given hashCode.
     * @param h
     * @return 
     */
    static int hash(int h){
        h ^= (h >>> 20) ^ (h >>> 12);
        return h ^ (h >>> 7) ^ (h >>> 4);
    }
    
    static int indexFor(int h, int length) {
        return h & (length - 1);
    }
    
    @Override
    public int size(){
        return size;
    }
    
    
    /**
     * Removes all of the mappings from this map.
     */
    @Override
    public void clear() {
        Entry[] tab = table;
        for(int i=0; i<tab.length; i++)
            tab[i] = null;
        size = 0;
    }

    @Override
    public boolean containsKey(Object key) {
        return getEntry(key) != null;
    }

    @Override
    public boolean containsValue(Object value) {
        if(value == null)
            return containsNullValue();
        
        Entry[] tab = table;
        for(int i=0; i<tab.length; i++)
            for(Entry e = tab[i]; e != null; e = e.next)
                if(value.equals(e.value))
                    return true;
        return false;
    }

    @Override
    public AbstractSet entrySet() {
        AbstractSet es = entrySet;
        return es != null? es: (entrySet = new EntrySet());
    }

    @Override
    public Object get(Object key) {
        if(key == null)
            // get null key is not yet supported
            throw new CororException("get by a null key is not yet supported");
        int hash = hash(key.hashCode());
        for(Entry e = table[indexFor(hash, table.length)]; e != null; e = e.next) {
            Object k = null;
            if(e.hash == hash && ((k = e.key) == key || key.equals(k))) return e.value;
        }
        return null;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    public AbstractSet keySet() {
        AbstractSet ks = keySet;
        return (ks != null ? ks : (keySet = new KeySet()));
    }

    @Override
    public Object put(Object key, Object value) {
        if(key == null) throw new CororException("Null key is not yet supported");
        int hash = hash(key.hashCode());
        int i = indexFor(hash, table.length);
        for(Entry e = table[i]; e != null; e = e.next){
            Object k = null;
            if(e.hash == hash && ((k = e.key) == key || key.equals(k)) ){
                Object oldValue = e.value;
                e.value = value;
                return oldValue;
            }
        }
        
        addEntry(hash, key, value, i);
        return null;
    }

    @Override
    public void putAll(Map m) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * removes the mapping for the specified key from the map if present.
     * @param key key whose mapping is to be removed from the map
     * @return the previous value associated with key, or null if there was no mapping for key.
     */
    @Override
    public Object remove(Object key) {
        Entry e = removeEntryFromKey(key);
        return (e == null ? null : e.value);
    }

    @Override
    public AbstractCollection values() {
        AbstractCollection vs = values;
        return (vs != null ? vs : (values = new Values()));
    }

    void addEntry(int hash, Object key, Object value, int i) {
        Entry e = table[i];
        table[i] = new Entry(hash, key, value, e);
        if(size++ >= table.length * loadFactor)
            resize(2 * table.length);
    }
    
    void resize(int newCapacity){
        Entry[] oldTable = table;
        int oldCapacity = oldTable.length;
        if(oldCapacity == MAXIMUM_CAPACITY){
            return;
        }
        
        Entry[] newTable = new Entry[newCapacity];
        transfer(newTable);
        table = newTable;
    }

    void transfer(Entry[] newTable) {
        Entry[] src = table;
        int newCapacity = newTable.length;
        for(int j=0; j<src.length; j++) {
            Entry e = src[j];
            if(e != null){
                src[j] = null;
                do {
                    Entry next = e.next;
                    int i = indexFor(e.hash, newCapacity);
                    e.next = newTable[i];
                    newTable[i] = e;
                    e = next;
                } while(e != null);
            }
        }
    }

    Entry removeEntryFromKey(Object key) {
        if(key == null) throw new CororException("a null key is not yet supported");
        int hash = hash(key.hashCode());
        int i = indexFor(hash, table.length);
        Entry prev = table[i];
        Entry e = prev;
        
        while(e != null){
            Entry next = e.next;
            Object k;
            if(e.hash == hash && ((k = e.key) == key || (key.equals(k)))) {
                size --;
                if(prev == e)
                    table[i] = next;
                else 
                    prev.next = next;
                
                return e;
            }
            prev = e;
            e = next;
        }
        return e;
    }

    boolean containsNullValue() {
        
        Entry[] tab = table;
        for(int i=0; i<tab.length; i++)
            for(Entry e = tab[i]; e != null; e = e.next)
                if(e.value == null)
                    return true;
        return false;    }

    private Object getEntry(Object key) {
        if(key == null) throw new CororException("null key is not yet supported");
        int hash = hash(key.hashCode());
        for(Entry e = table[indexFor(hash, table.length)]; e != null; e = e.next){
            Object k;
            if(e.hash == hash && ((k = e.key) == key || key.equals(k))) return e;
        }
        return null;
    }
    
    private static class Entry implements Map.Entry{
        
        Object key;
        
        Object value;
        
        final int hash;
        
        Entry next;
        
        Entry(int hash, Object key, Object value, Entry next){
            this.key = key;
            this.value = value;
            this.hash = hash;
            this.next = next;
        }
        
        public final boolean equals(Object o){
            if(! (o instanceof Map.Entry)) return false;
            Map.Entry e = (Map.Entry) o;
            Object k1 = key;
            Object k2 = e.getKey();
            
            if(k1 == k2 || (k1 != null && k1.equals(k2))){
                Object v1 = key;
                Object v2 = e.getValue();
                if(v1 == v2 || (v1 != null && v1.equals(v2)))
                    return true;
            }
            return false;
        }
        
        public final int hashCode(){
            return ((key == null ? 0:key.hashCode()) ^ 
                    (value == null ? 0:value.hashCode()));
        }
        
        public final String toString(){
            return getKey() + "=" + getValue();
        }

        @Override
        public Object getKey() {
            return key;
        }

        @Override
        public Object getValue() {
            return value;
        }

        @Override
        public void setKey(Object newKey) {
            key = newKey;
        }

        @Override
        public void setValue(Object newValue) {
            value = newValue;
        }
    }
    
    private class KeySet extends AbstractSet {
        public Iterator iterator(){
            return new HashIterator(){
                public Object next() throws CororException {
                    return nextEntry().getKey();
                }

				@Override
				public void reset() {
					throw new UnsupportedOperationException("Not supported yet.");
					
				}                
            };
        }
        
        public int size() {
            return size;
        }
        
        public boolean contains(Object c){
            return containsKey(c);
        }
        
        public boolean remove(Object o){
            return HashMap.this.removeEntryFromKey(o) != null;
        }
        
        public void clear(){
            HashMap.this.clear();
        }

        @Override
        public boolean add(Object o) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }
    }
    
    private final class Values extends AbstractCollection{

        @Override
        public boolean equals(Object o) {
            if(o == this) return true;
            if(!(o instanceof Values)) return false;
            AbstractCollection c = (AbstractCollection)o;
            if(c.size() != size()) return false;
            return containsAll(c);
        }

        @Override
        public int hashCode() {
            int hash = 0;
            Iterator i = iterator();
            while(i.hasNext()){
                Object o = i.next();
                if(o != null)
                    hash += o.hashCode();
            }
            return hash;
        }

        @Override
        public Iterator iterator() {
            return new HashIterator(){
                public Object next() throws CororException {
                    return nextEntry().getValue();
                }

				@Override
				public void reset() {
					throw new UnsupportedOperationException("Not supported yet.");
					
				}
            };
        }

        public boolean contains(Object o){
            return containsValue(o);
        }
        
        @Override
        public int size() {
            return size;
        }
        
        public void clear(){
            HashMap.this.clear();
        }

        @Override
        public boolean add(Object o) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }
        
    }
    
    private abstract class HashIterator implements Iterator {
        Entry next;
        int index;
        Entry current;
        
        HashIterator(){
            if(size > 0) {
                Entry[] t = table;
                while(index < t.length && (next = t[index++]) == null) ;
            }
        }
        
        public final boolean hasNext(){
            return next != null;
        }
        
        final Entry nextEntry(){
            Entry e = next;
            if(e == null) throw new CororException("No such element exception");
            
            // if the next entry in the entry list is null then point index to next entry slot with a null entry
            if((next = e.next) == null){
                Entry [] t = table;
                while(index < t.length && (next = t[index++]) == null) ;
            }
            
            current = e;
            return e;
        }
        
        public void remove(){
            if(current == null) throw new CororException("Illegal state exception");
            Object k = current.key;
            current = null;
            HashMap.this.removeEntryFromKey(k);
        }
    }
    
    private final class EntrySet extends AbstractSet {

        @Override
        public Iterator iterator() {
            return new HashIterator(){

                @Override
                public Object next() throws CororException {
                    return nextEntry();
                }

				@Override
				public void reset() {
					throw new UnsupportedOperationException("Not supported yet.");
					
				}
                
            };
        }

        @Override
        public int size() {
            return size;
        }
        
        public boolean contains(Object o){
            if(!(o instanceof Map.Entry)) return false;
            Map.Entry e = (Map.Entry) o;
            Entry candidate = getEntry(e.getKey());
            return candidate != null && candidate.equals(e);
        }
        
        public boolean remove(Object o){
            return removeMapping(o) != null;
        }
        
        public void clear(){
            HashMap.this.clear();
        }

        private Entry getEntry(Object key) {
            if(key == null) throw new CororException("null key is not yet supported");
            int hash = key.hashCode();
            for(Entry e = table[indexFor(hash, table.length)]; e != null; e = e.next) {
                Object k;
                if(e.hash == hash && ((k = e.key) == key || key.equals(k))) return e;
            }
            return null;
        }

        private Object removeMapping(Object o) {
            if(! (o instanceof Map.Entry)) return null;
            
            Map.Entry entry = (Map.Entry) o;
            Object key = entry.getKey();
            if(key == null) throw new CororException("null key is not yet supported");
            int hash = hash(key.hashCode());
            int i = indexFor(hash, table.length);
            Entry prev = table[i];
            Entry e = prev;
            
            while(e != null){
                Entry next = e.next;
                if(e.hash == hash && e.equals(entry)) {
                    size --;
                    if(prev == e) table[i] = next;
                    else prev.next = next;
                    return e;
                }
                prev = e;
                e = next;
            }
            
            return e;
        }

        @Override
        public boolean add(Object o) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }
        
    }
    
}
