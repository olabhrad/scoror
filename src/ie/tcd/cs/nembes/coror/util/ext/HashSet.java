/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ie.tcd.cs.nembes.coror.util.ext;

import ie.tcd.cs.nembes.coror.util.Iterator;

/**
 *
 * @author WEI TAI
 */
public class HashSet extends AbstractSet{
    private HashMap map;
    
    private static final Object PRESENT = new Object();
    
    public HashSet() {
        map = new HashMap();
    }
    
    public HashSet(int initialCapacity, float loadFactor){
        map = new HashMap(initialCapacity, loadFactor);
    }
    
    public HashSet(int initialCapacity) {
        map = new HashMap(initialCapacity);
    }
    
    public Iterator iterator(){
        return map.keySet.iterator();
    }
    
    public int size(){
        return map.size;
    }
    
    public boolean isEmpty(){
        return map.isEmpty();
    }
    
    public boolean contains(Object o){
        return map.containsKey(o);
    }
    
    public boolean add(Object e){
        return map.put(e, PRESENT) == null;
    }
    
    public boolean remove(Object o){
        return map.remove(o) == PRESENT;
    }
    
    public void clear(){
        map.clear();
    }
}
