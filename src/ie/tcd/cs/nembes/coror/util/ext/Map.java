/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ie.tcd.cs.nembes.coror.util.ext;

import ie.tcd.cs.nembes.coror.shared.CororException;
import ie.tcd.cs.nembes.coror.util.ArraySet;
import ie.tcd.cs.nembes.coror.util.iterator.IteratorImpl;
import java.util.Vector;

/**
 * A map interface
 * @author WEI TAI
 */
public interface Map {
    
    
    
    public void clear() ;
    
    public boolean containsKey(Object key) ;
    
    public boolean containsValue(Object value) ;
    
    public AbstractSet entrySet();
    
    public boolean equals(Object other) ;
    
    public Object get(Object key) ;
    
    public boolean isEmpty() ;
        
    public AbstractSet keySet();

    public Object put(Object key, Object value) ;
    
    public void putAll(Map m) ;
    
    public Object remove(Object searchKey) ;
    
    public int size() ;
    
    public AbstractCollection values();
    
    public interface Entry {

	public Object getKey();

	public Object getValue() ;

	public void setKey(Object newKey) ;

	public void setValue(Object newValue) ;

	public boolean equals(Object other) ;
    }
}
