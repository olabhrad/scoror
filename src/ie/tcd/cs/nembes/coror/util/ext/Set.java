/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ie.tcd.cs.nembes.coror.util.ext;

import ie.tcd.cs.nembes.coror.util.Iterator;

/**
 * An abstract class for set. Analogous to AbstractSet in JDK
 * @author WEI TAI
 */
public abstract class Set extends Collection{
    
    public boolean removeAll(Collection c){
        boolean modified = false;
        
        if(size() > c.size()) {
            for(Iterator i = c.iterator(); i.hasNext(); )
                modified |= remove(i.next());
        } else {
            for(Iterator i = iterator(); i.hasNext(); ){
                if(c.contains(i.next())){
                    i.remove();
                    modified = true;
                }
            }
        }
        return modified;
    }
    
    public boolean equals(Object o){
        if (o == this)
            return true;
        if(! (o instanceof Set)) return false;
        Collection c = (Collection) o;
        if(c.size() != size()) return false;
        return containsAll(c);
    }
    
    public int hashCode(){
        int h = 0;
        Iterator i = iterator();
        while(i.hasNext()){
            Object obj = i.next();
            if(obj != null)
                h += obj.hashCode();
        }
        return h;
    }
    
}
