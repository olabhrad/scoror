package ie.tcd.cs.nembes.coror.util.iterator;

import ie.tcd.cs.nembes.coror.util.Iterator;
import ie.tcd.cs.nembes.coror.util.ArrayList;
import ie.tcd.cs.nembes.coror.util.ArraySet;


public interface ExtendedIterator extends Iterator{
	/**
	    return a new iterator which delivers all the elements of this iterator and
	    then all the elements of the other iterator. Does not copy either iterator;
	    they are consumed as the result iterator is consumed.
	*/
	public ExtendedIterator andThen( ExtendedIterator other );
	
	/**
	    Answer a list of the [remaining] elements of this iterator, in order,
	    consuming this iterator.
	*/
	public ArrayList toList();
	
	/**
	   Answer a set of the [remaining] elements of this iterator, in order,
	   consuming this iterator.
	*/
	public ArraySet toSet();
	
        /**
            return a new iterator containing only the elements of _this_ which
            are rejected by the filter _f_. The order of the elements is preserved.
            Does not copy _this_, which is consumed as the reult is consumed.
	*/
//	public ExtendedIterator filterDrop( Filter f );
        
        /** 
            Close the iterator. Other oeprations on this iterator may now throw an exception. A
            ClosableIterator may be closed as many times as desired - the subsequent
            calls do nothing.
        */
        public void close();
}
