/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tobedeleted;

import ie.tcd.cs.nembes.coror.graph.Node;
import ie.tcd.cs.nembes.coror.util.ArrayMap;
import ie.tcd.cs.nembes.coror.util.ArrayList;
import ie.tcd.cs.nembes.coror.util.MultiArrayMap;
import ie.tcd.cs.nembes.coror.util.ArraySet;
import java.lang.UnsupportedOperationException;


/**
 * A buffer allowing faster list accessed. It is designed to cache lists at 
 * ontology loading time. It can also be called in rule builtins for list processing
 * such as listMapAsSubject, listMapAsObject, listMapAsPair, etc.  
 * @author WEI TAI
 * @deprecated temporarily deprecated
 */
public final class RDFListBuffer {
    
    /** singleton instance of list buffer */
    private static RDFListBuffer instance;
    
    /** the buffer where list members are kept */
    private MultiArrayMap buffer;
    
    /** a map with key as head and value as the next rest to be added */
    private ArrayMap restIndex;
    
    private RDFListBuffer(){}
    
    public static RDFListBuffer getBuffer(){
        if(instance == null) instance = new RDFListBuffer();
        return instance;
    }
    
    public void addElement(Node rest, Node element){
        
        // search for the list
        ArraySet keys = restIndex.keySet();
        for(byte i=0; i<keys.size(); i++){
            if(restIndex.get(keys.get(i)).equals(rest))
                buffer.put(keys.get(i), element);
        }
    }
    
    /**
     * SetImpl the next rest node to be linked for a list.
     * @param prvRest the previous rest node
     * @param nextRest the next rest node to be linked
     */
    public void setNextRest(Node prvRest, Node nextRest){
        ArraySet keys = restIndex.keySet();
        for(byte i=0; i<keys.size(); i++){
            if(restIndex.get(keys.get(i)).equals(prvRest))
                restIndex.put(keys.get(i), nextRest);
        }
    }
    
    public boolean listContains(Node head, Node element){
        throw new UnsupportedOperationException("Not implemented");
    }
    
    public ArrayList getList(Node head){
        return buffer.getAllAsList(head);
    }
}
