package tobedeleted;


import ie.tcd.cs.nembes.coror.graph.Graph;
import ie.tcd.cs.nembes.coror.reasoner.InfGraph;
import ie.tcd.cs.nembes.coror.shared.CororException;
import ie.tcd.cs.nembes.coror.util.ArrayList;

/**
 * This is an optimized version of the original RETE engine that 
 * other Jena reasoner adopted. 
 * 
 * @deprecated
 * the reasoner classes is only for generating InfGraph in Coror,
 * but has nothing to do with the RETE engine. It is removed from Coror. bindSchema
 * and bind need to be commented back to reuse this class.
 * @author Wei Tai
 *
 */
public class RETEReasoner implements Reasoner {

    /** The rules to be used by this instance of the forward engine */
    protected ArrayList rules = new ArrayList();
    
    /** A precomputed set of schema deductions */
    protected Graph schemaGraph;
    
    /**
     * Constructor. This is the raw version that does not reference a ReasonerFactory
     * and so has no capabilities description. 
     * @param rules a list of Rule instances which defines the ruleset to process
     */
    public RETEReasoner(ArrayList rules) {
        if (rules == null) throw new NullPointerException( "null rules" );
        this.rules = rules;
    }
    
    /**
     * Internal constructor, used to generated a partial binding of a schema
     * to a rule reasoner instance.
     */
    protected RETEReasoner(ArrayList rules, Graph schemaGraph) {
        this(rules);
        this.schemaGraph = schemaGraph;
    }

    /**
         Add the given rules to the current set and answer this Reasoner. Provided 
         so that the Factory can deal out reasoners with specified rulesets. 
         There may well be a better way to arrange this.
         TODO review & revise
    */
    public RETEReasoner addRules(ArrayList rules) {
        ArrayList combined = new ArrayList( this.rules );
        combined.addAll( rules );
        setRules( combined );
        return this;
        }
    
    /**
     * Precompute the implications of a schema graph. The statements in the graph
     * will be combined with the data when the final InfGraph is created.
     */
    @Override
    public Reasoner bindSchema(Graph tbox) throws CororException {
        // comment back when RETEReasoner is in use
//        if (schemaGraph != null) {
//            throw new CororException ("Can only bind one schema at a time to a GenericRuleReasoner");
//        }
//        schemaGraph = tbox;
//        Graph graph = new BasicForwardRuleInfGraph(this, rules, tbox, null);
//        ((InfGraph)graph).prepare();
//        RETEReasoner efr = new RETEReasoner(rules, graph);
//        
//        return efr;
        return null;
    }

    /**
     * Attach the reasoner to a set of RDF data to process.
     * The reasoner may already have been bound to specific rules or ontology
     * axioms (encoded in RDF) through earlier bindRuleset calls.
     * 
     * @param data the RDF data to be processed, some reasoners may restrict
     * the range of RDF which is legal here (e.g. syntactic restrictions in OWL).
     * @return an inference graph through which the data+reasoner can be queried.
     * @throws ReasonerException if the data is ill-formed according to the
     * constraints imposed by this reasoner.
     */
    @Override
    public InfGraph bind( Graph data ) throws CororException {
        // comment back this method when RETEReasoner is in use again
//        BasicForwardRuleInfGraph graph = new BasicForwardRuleInfGraph(this, rules, schemaGraph, data);
//        return graph;
        return null;
    }
    
    /**
     * Return the list of Rules used by this reasoner
     * @return a List of Rule objects
     */
    @Override
    public ArrayList getRules() {
        return rules;
    } 

    /**
     * Set (or change) the rule set that this reasoner should execute.
     * @param rules a list of Rule objects
     */
    @Override
    public void setRules(ArrayList rules) {
        this.rules = rules;
        if (schemaGraph != null) {
            // The change of rules invalidates the existing precomputed schema graph
            // This might be recoverable but for now simply flag the error and let the
            // user reorder their code to set the rules before doing a bind!
            throw new CororException("Cannot change the rule set for a bound rule reasoner.\nSet the rules before calling bindSchema");
        }
    }
}
